<?php $my_path = drupal_get_path('theme', 'plumacreativa'); ?>
<!-- stylesheet switcher begin -->
    <!-- stylesheet switcher end -->
    <div id="mobile-nav" class="visible-xs">
      <div class="" id="flyout-container">
        <ul class="nav flyout-menu">
          <li class="reorder"><a href="#" title=""><i class="icon-reorder"></i></a></li>
          <li>
            <a title="" href="#"><i class="icon-globe"></i> Home</a>
          </li>
          <li>
            <a title="" href="#"><i class="icon-pencil"></i> Blog</a>
          </li>
          <li>
            <a title="" href="#"><i class="icon-camera"></i> Portfolio</a>
          </li>
          <li>
            <a title="" href="#"><i class="icon-cog"></i> Shortcodes</a>
          </li>
          <li>
            <a title="" href="#"><i class="icon-dollar"></i> Shop</a>
          </li>
          <li>
            <a title="" href="#"><i class="icon-envelope-alt"></i> Contacts</a>
          </li>
          <li>
            <a title="" href="#"><i class="icon-camera"></i> Portfolio</a>
          </li>
          <li>
            <a title="" href="#"><i class="icon-cog"></i> Shortcodes</a>
          </li>
          <li>
            <a title="" href="#"><i class="icon-dollar"></i> Shop</a>
          </li>
          <li>
            <a title="" href="#"><i class="icon-envelope-alt"></i> Contacts</a>
          </li>
          <li>
            <a title="" href="#"><i class="icon-camera"></i> Portfolio</a>
          </li>
          <li>
            <a title="" href="#"><i class="icon-cog"></i> Shortcodes</a>
          </li>
          <li>
            <a title="" href="#"><i class="icon-dollar"></i> Shop</a>
          </li>
          <li>
            <a title="" href="#"><i class="icon-envelope-alt"></i> Contacts</a>
          </li>
        </ul>
      </div>
    </div>

    <div class="container full-bg visible-xs fixed-nav">
      <nav>
        <div class="row navbar">
          <div class="navbar-inner">
            <ul class="main-menu nav">
              <li><img src="<?php print $my_path ?>/img/logotype.png"/></li>
              <li class="reorder"><a href="#" title=""><i class="icon-reorder"></i></a></li>
              <li class="nav-search"><a href="#" title=""><i class="icon-search"></i></a><li>
            </ul>
          </div>
        </div>
        <div class="mobile-search">
          <form>
            <input type="text" placeholder="Type your search phrase..." />
          </form>
        </div>
      </nav>
    </div>

    <div id="site-wrapper">
      <header id="ha-header" class="ha-header ha-header-fullscreen">
        <div class="ha-header-perspective">
          <header class="navbar top ha-waypoint main-header">
            <div class="back-to-splash"></div>
              <div class="container">
                <nav>
                  <div class="row">
                    <figure>
                      <a href="#" id="back-to-splash"><img src="<?php print $my_path ?>/img/logo.png"></a>
                    </figure>
                    <div id="main-nav">
                      <ul class="nav">
                        <li class="active">
                          <a title="" href="#home">Home</a>
                        </li>
                        <li class="active">
                          <a title="" href="#nosotros">Nosotros</a>
                        </li>
                        <li class="active">
                          <a title="" href="#servicios">Servicios</a>
                        <li>
                          <a title="" href="#portfolio">Portfolio</a>
                        </li>
                        <li>
                          <a title="" href="#team">Team</a>
                        </li>
                        <li>
                          <a title="" href="#testimonials">Testimonials</a>
                        </li>
                        <li>
                          <a title="" href="#blog">Blog</a>
                        </li>
                        <li>
                          <a title="" href="#contact">Contact</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </nav>
              </div>
              <div class="edge"></div>
              <div class="progressbar"></div>
            </header>
          </div>
        </header>
        <div class="container first-container ha-waypoint" data-animate-down="ha-header-subfullscreen" data-animate-up="ha-header-fullscreen" id="home">
          <div class="intro anim1">
            <div id="slides">
              <?php
                $block = module_invoke('plumacreativa', 'block_view', 'slide');
                print $block['content'];
              ?>
              <nav class="slides-navigation">
                <a href="#" class="next"><i class="icon-chevron-right"></i></a>
                <a href="#" class="prev"><i class="icon-chevron-left"></i></a>
              </nav>
            </div>
          </div>
        </div>
        <div id="limpieza">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <?php
                $block = module_invoke('views', 'block_view', 'limpieza_home-block');
                print render($block['content']);
              ?>
            </div>
          </div>
        </div>
        <div id="nosotros">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <?php $node = node_load(27); ?>
              <div class="field-title"><?php print $node->title ?></div>
              <div class="field-body"><?php print $node->body['und'][0]['value'] ?></div>
            </div>
          </div>
        </div>

        <div id="servicios" class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <?php
                $block = module_invoke('views', 'block_view', 'lista_de_servicios-block');
                print render($block['content']);
              ?>
            </div>
          </div>
        </div>

        <!-- Team -->
        <div class="container team" id="team">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="banner">
                <h3>Team</h3>
                <div class="edge"></div>
                <div class="progressbar"></div>
              </div>
            </div>
          </div>
          <div class="">
            <?php
              $block = module_invoke('plumacreativa', 'block_view', 'team');
              print $block['content'];
            ?>
          </div>
        </div>

        <!-- Testimonials -->
        <!--<div class="container testimonials" id="testimonials">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="banner">
                <h3>Clients testimonials</h3>
                <div class="navarrows">
                  <a id="testimonialsPrev" title="prev" href="#"><i class="icon-chevron-left"></i></a>
                  <a id="testimonialsNext" title="next" href="#"><i class="icon-chevron-right"></i></a>
                </div>
                <div class="edge"></div>
                <div class="progressbar"></div>
              </div>
            </div>
          </div>
          <div class="row testimonials-container mobile-slider-wrapper">
            <div id="testimonialsSlide" class="mobile-slider">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 sliderItemWrapp anim1">
                <div class="col-xs-5 col-sm-5 col-md-5">
                  <figure class="photo-people rot hover-img">
                    <img src="http://placehold.it/120x120" alt="" />
                    <img src="http://placehold.it/120x120" alt="" />
                  </figure>
                  <div class="data-person">
                    <strong>John Doe</strong>
                    <span>Microsoft inc. developer and ingeneer</span>
                  </div>
                </div>
                <div class="col-xs-7 col-sm-7 col-md-7">
                  <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "</p>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 sliderItemWrapp anim2">
                <div class="col-xs-5 col-sm-5 col-md-5">
                  <figure class="photo-people rot hover-img">
                    <img src="http://placehold.it/120x120" alt="" />
                    <img src="http://placehold.it/120x120" alt="" />
                  </figure>
                  <div class="data-person">
                    <strong>Katie Link</strong>
                    <span>Microsoft inc. developer and ingeneer</span>
                  </div>
                </div>
                <div class="col-xs-7 col-sm-7 col-md-7">
                  <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 sliderItemWrapp">
                <div class="col-xs-5 col-sm-5 col-md-5">
                  <figure class="photo-people rot hover-img">
                    <img src="http://placehold.it/120x120" alt="" />
                    <img src="http://placehold.it/120x120" alt="" />
                  </figure>
                  <div class="data-person">
                    <strong>John Doe</strong>
                    <span>Microsoft inc. developer and ingeneer</span>
                  </div>
                </div>
                <div class="col-xs-7 col-sm-7 col-md-7">
                  <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "</p>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 sliderItemWrapp">
                <div class="col-xs-5 col-sm-5 col-md-5">
                  <figure class="photo-people rot hover-img">
                    <img src="http://placehold.it/120x120" alt="" />
                    <img src="http://placehold.it/120x120" alt="" />
                  </figure>
                  <div class="data-person">
                    <strong>Katie Link</strong>
                    <span>Microsoft inc. developer and ingeneer</span>
                  </div>
                </div>
                <div class="col-xs-7 col-sm-7 col-md-7">
                  <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 sliderItemWrapp">
                <div class="col-xs-5 col-sm-5 col-md-5">
                  <figure class="photo-people rot hover-img">
                    <img src="http://placehold.it/120x120" alt="" />
                    <img src="http://placehold.it/120x120" alt="" />
                  </figure>
                  <div class="data-person">
                    <strong>John Doe</strong>
                    <span>Microsoft inc. developer and ingeneer</span>
                  </div>
                </div>
                <div class="col-xs-7 col-sm-7 col-md-7">
                  <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "</p>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 sliderItemWrapp">
                <div class="col-xs-5 col-sm-5 col-md-5">
                  <figure class="photo-people rot hover-img">
                    <img src="http://placehold.it/120x120" alt="" />
                    <img src="http://placehold.it/120x120" alt="" />
                  </figure>
                  <div class="data-person">
                    <strong>Katie Link</strong>
                    <span>Microsoft inc. developer and ingeneer</span>
                  </div>
                </div>
                <div class="col-xs-7 col-sm-7 col-md-7">
                  <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                </div>
              </div>
            </div>
          </div>
        </div>-->
        <div class="container clients" id="clients">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="banner">
                <h3>Clientes</h3>
                <div class="navarrows">
                  <a id="clientPrev" title="prev" href="#"><i class="icon-chevron-left"></i></a>
                  <a id="clientNext" title="next" href="#"><i class="icon-chevron-right"></i></a>
                </div>
                <div class="edge"></div>
                <div class="progressbar"></div>
              </div>
            </div>
          </div>
          <div class="row clients-container mobile-slider-wrapper">
            <div id="clientSlide" class="mobile-slider">
              <div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp anim1">
                <figure>
                  <img src="http://placehold.it/130x40" class="client-hover"/>
                </figure>
              </div>
              <div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp anim1">
                <figure>
                  <img src="http://placehold.it/130x40" class="client-hover">
                </figure>
              </div>
              <div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp anim1">
                <figure>
                  <img src="http://placehold.it/130x40" class="client-hover"/>
                </figure>
              </div>
              <div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp anim1">
                <figure>
                  <img src="http://placehold.it/130x40" class="client-hover"/>
                </figure>
              </div>
              <div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp anim1">
                <figure>
                  <img src="http://placehold.it/130x40" class="client-hover"/>
                </figure>
              </div>
              <div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp anim1">
                <figure>
                  <img src="http://placehold.it/130x40" class="client-hover"/>
                </figure>
              </div>
              <div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp">
                <figure>
                  <img src="http://placehold.it/130x40" class="client-hover"/>
                </figure>
              </div>
              <div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp">
                <figure>
                  <img src="http://placehold.it/130x40" class="client-hover"/>
                </figure>
              </div>
              <div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp">
                <figure>
                  <img src="http://placehold.it/130x40" class="client-hover"/>
                </figure>
              </div>
              <div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp">
                <figure>
                  <img src="http://placehold.it/130x40" class="client-hover"/>
                </figure>
              </div>
              <div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp">
                <figure>
                  <img src="http://placehold.it/130x40" class="client-hover"/>
                </figure>
              </div>
              <div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp">
                <figure>
                  <img src="http://placehold.it/130x40" class="client-hover"/>
                </figure>
              </div>
            </div>
          </div>
        </div>
        <!-- Fin de Clientes -->

        <div class="container blog" id="blog">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="banner">
                <h3>Blog</h3>
                <div class="edge"></div>
                <div class="progressbar"></div>
                <ul class="section-nav">
                  <li><a href="#" alt="" class="blog-selected" data-blog="1">all</a></li>
                  <li><a href="#" alt="" data-blog="2">january</a></li>
                  <li><a href="#" alt="" data-blog="3">february</a></li>
                  <li><a href="#" alt="" data-blog="1">march</a></li>
                  <li><a href="#" alt="" data-blog="2">april</a></li>
                  <li><a href="#" alt="" data-blog="3">may</a></li>
                  <li><a href="#" alt="" data-blog="1">june</a></li>
                  <li><a href="#" alt="" data-blog="2">july</a></li>
                  <li><a href="#" alt="" data-blog="3">august</a></li>
                  <li><a href="#" alt="" data-blog="1">september</a></li>
                  <li><a href="#" alt="" data-blog="2">october</a></li>
                  <li><a href="#" alt="" data-blog="3">november</a></li>
                  <li><a href="#" alt="" data-blog="1">december</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="row blog-content anim1">
            <div class="grid-sizer"></div>
            <div class="article-content article-content-h1">
              <div class="img-art">
                <a href="#" class="hover-post">
                  <figure>
                    <img src="http://placehold.it/280x195" alt=""/>
                  </figure>
                </a>
                <div class="date-art">17.02</div>
              </div>
              <div class="title-art">
                <a href="#"><h1>Lorem ipsum</h1></a>
                <a href="#"><small>by Admin</small></a>
                <ul class="blog-tags">
                  <li><a href="#" alt="">wordpress</a></li>
                  <li><a href="#" alt="">programing</a></li>
                  <li><a href="#" alt="">design</a></li>
                </ul>
              </div>
              <div class="spacing"></div>
              <div class="articles">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
            </div>
            <div class="article-content article-content-h1">
              <div class="img-art">
                <a href="#" class="hover-post">
                  <figure>
                    <img src="http://placehold.it/280x195" alt=""/>
                  </figure>
                </a>
                <div class="date-art">16.02</div>
              </div>
              <div class="title-art">
                <a href="#"><h1>Lorem ipsum</h1></a>
                <a href="#"><small>by Admin</small></a>
                <ul class="blog-tags">
                  <li><a href="#" alt="">no name</a></li>
                  <li><a href="#" alt="">black</a></li>
                  <li><a href="#" alt="">white</a></li>
                  <li><a href="#" alt="">design</a></li>
                </ul>
              </div>
              <div class="spacing"></div>
              <div class="articles">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div>
            <div class="article-content article-content-h2">
              <div class="img-art">
                <a href="#" class="hover-post">
                  <figure>
                    <img src="http://placehold.it/280x410" alt=""/>
                  </figure>
                  <div class="date-art">15.02</div>
                </a>
              </div>
              <div class="title-art">
                <a href="#"><h1>Lorem ipsum</h1></a>
                <a href="#"><small>by Admin</small></a>
                <ul class="blog-tags">
                  <li><a href="#" alt="">wordpress</a></li>
                  <li><a href="#" alt="">design</a></li>
                </ul>
              </div>
              <div class="spacing"></div>
              <div class="articles">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
              </div>
            </div>
            <div class="article-content article-content-h1">
              <div class="img-art">
                <a href="#" class="hover-post">
                  <figure>
                    <img src="http://placehold.it/280x195" alt=""/>
                  </figure>
                </a>
                <div class="date-art">14.02</div>
              </div>
              <div class="title-art">
                <a href="#"><h1>Lorem ipsum</h1></a>
                <a href="#"><small>by Admin</small></a>
                <ul class="blog-tags">
                  <li><a href="#" alt="">wordpress</a></li>
                  <li><a href="#" alt="">design</a></li>
                </ul>
              </div>
              <div class="spacing"></div>
              <div class="articles">
                <p>Labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              </div>
            </div>
            <div class="article-content article-content-h1">
              <div class="img-art">
                <a href="#" class="hover-post">
                  <figure>
                    <img src="http://placehold.it/280x195" alt=""/>
                  </figure>
                </a>
                <div class="date-art">13.02</div>
              </div>
              <div class="title-art">
                <a href="#"><h1>Lorem ipsum</h1></a>
                <a href="#"><small>by Admin</small></a>
                <ul class="blog-tags">
                  <li><a href="#" alt="">car</a></li>
                  <li><a href="#" alt="">hot</a></li>
                  <li><a href="#" alt="">cake</a></li>
                  <li><a href="#" alt="">design</a></li>
                </ul>
              </div>
              <div class="spacing"></div>
              <div class="articles">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              </div>
            </div>
            <div class="article-content article-content-h2">
              <div class="img-art">
                <a href="#" class="hover-post">
                  <figure>
                    <img src="http://placehold.it/280x195" alt=""/>
                  </figure>
                </a>
                <div class="date-art">12.02</div>
              </div>
              <div class="title-art">
                <a href="#"><h1>Lorem ipsum</h1></a>
                <a href="#"><small>by Admin</small></a>
                <ul class="blog-tags">
                  <li><a href="#" alt="">car</a></li>
                  <li><a href="#" alt="">hot</a></li>
                  <li><a href="#" alt="">cake</a></li>
                  <li><a href="#" alt="">design</a></li>
                </ul>
              </div>
              <div class="spacing"></div>
              <div class="articles">
                <p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
              </div>
            </div>
            <div class="article-content article-content-h1">
              <div class="img-art">
                <a href="#" class="hover-post">
                  <figure>
                    <img src="http://placehold.it/280x195" alt=""/>
                  </figure>
                </a>
                <div class="date-art">11.02</div>
              </div>
              <div class="title-art">
                <a href="#"><h1>Lorem ipsum</h1></a>
                <a href="#"><small>by Admin</small></a>
                <ul class="blog-tags">
                  <li><a href="#" alt="">home</a></li>
                  <li><a href="#" alt="">hot</a></li>
                  <li><a href="#" alt="">lemon</a></li>
                  <li><a href="#" alt="">design</a></li>
                </ul>
              </div>
              <div class="spacing"></div>
              <div class="articles">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              </div>
            </div>

            <div class="article-content article-content-h1">
              <div class="img-art">
                <a href="#" class="hover-post">
                  <figure>
                    <img src="http://placehold.it/280x195" alt=""/>
                  </figure>
                </a>
                <div class="date-art">10.02</div>
              </div>
              <div class="title-art">
                <a href="#"><h1>Lorem ipsum</h1></a>
                <a href="#"><small>by Admin</small></a>
                <ul class="blog-tags">
                  <li><a href="#" alt="">city</a></li>
                  <li><a href="#" alt="">flower</a></li>
                  <li><a href="#" alt="">design</a></li>
                </ul>
              </div>
              <div class="spacing"></div>
              <div class="articles">
                <p>Lorem ipsum dolor sit amet, tempor incididunt ut labore et dolore magna aliqua. Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row blog-show-more-container">
          <button type="button" class="btn btn-default" id="blog-show-more">Show more...</button>
        </div>
        <div class="hidden blog-more-content"></div>
        <div class="container contact" id="contact">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="banner">
                <h3>Contact</h3>
                <div class="edge"></div>
                <div class="progressbar"></div>
              </div>
            </div>
          </div>
          <div class="row contact-container">
            <div class="col-xs-12 col-sm-6 col-md-3">
              <ul class="data-company anim1">
                <li>Company</li>
                <li><b>Lorem Ipsum</b></li>
              </ul>
              <ul class="data-company anim1">
                <li>Adress</li>
                <li>Armii Krajowej 17</li>
                <li>Świdnica</li>
              </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
              <ul class="data-company anim1">
                <li>Mobile</li>
                <li>+11 264 5 415739</li>
              </ul>
              <ul class="data-company anim1">
                <li>E-mail</li>
                <li>torresmatiasangel@gmail.com</li>
                <li>plumacreativa@gmail.com</li>
              </ul>
            </div>
            <form id="contact-form" class="anim1" action="inc/send.php" method="POST">
              <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="input-wrapper">
                  <input class="contact-form" type="text" placeholder="Name" name="form-name" id="form-name">
                  <span id="errorName" class="hidden error">Fill up name</span>
                </div>
                <div class="input-wrapper">
                  <input class="contact-form" type="text" placeholder="E-mail" name="form-email" id="form-email"><div class="clearfix visible-xs"></div>
                  <span id="errorEmail" class="hidden error">Fill up e-mail</span>
                </div>
                <div class="clearfix visible-xs"></div>
                <div class="input-wrapper">
                  <input class="contact-form" type="text" placeholder="Subject" name="form-subject" id="form-subject"><div class="clearfix visible-xs"></div>
                  <span id="errorSubject" class="hidden error">Fill up subject</span>
                </div>
                <div class="clearfix visible-xs"></div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="message">
                  <div class="input-wrapper">
                    <label for="text-message">Message</label>
                    <div class="clearfix visible-xs"></div>
                    <textarea class="contact-form" placeholder="Your message..." name="form-message" id="form-message"></textarea>
                    <span id="errorMessage" class="hidden error">Fill up message</span>
                  </div>
                </div>
                <div>
                  <input type="submit" value="Send" class="buttonform">
                </div>
                <div class="alert alert-success hidden">Your message was sent. Thank you!</div>
              </div>
            </form>
          </div>
        </div>
        <div class="full-bg feed">
          <div class="container ">
            <div class="row">
              <div class="banner col-xs-12 col-sm-12 col-md-12 feed-container mobile-slider-wrapper">
                <div id="feedSlide" class="mobile-slider">
                  <div class="sliderItemWrapp">
                    <ul class="media-list">
                      <li class="media">
                        <a class="pull-left" href="#" title="">
                          <i class="icon-twitter"></i>
                        </a>
                        <div class="media-body">
                          <h2 class="media-heading">RT  <a href='https://twitter.com/Japh'>@Japh</a> Waiting for day 2 to commence  <a href='https://twitter.com/search?q=%23wceu&src=hash'>#wceu</a> @ WordCamp Europe <a href='http://t.co/bTVY33ag22' target='_blank'>http://t.co/bTVY33ag22</a></h2>
                          <p>5 hours ago</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="sliderItemWrapp">
                    <ul class="media-list">
                      <li class="media">
                        <a class="pull-left" href="#" title="">
                          <i class="icon-twitter"></i>
                        </a>
                        <div class="media-body">
                          <h2 class="media-heading">I’m at #wceu, and will be sharing some photos with you. I also have a few Envato and Microlancer t-shirts left. Come find me! ^@Japh</h2>
                          <p>1 day ago</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="sliderItemWrapp">
                    <ul class="media-list">
                      <li class="media">
                        <a class="pull-left" href="#" title="">
                          <i class="icon-twitter"></i>
                        </a>
                        <div class="media-body">
                          <h2 class="media-heading">RT  <a href='https://twitter.com/Japh'>@Japh</a> Zé introducing  <a href='https://twitter.com/search?q=%23wceu&src=hash'>#wceu</a> @ WordCamp Europe <a href='http://t.co/DyN0QvAKvz' target='_blank'>http://t.co/DyN0QvAKvz</a></h2>
                          <p>1 day ago</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="sliderItemWrapp">
                    <ul class="media-list">
                      <li class="media">
                        <a class="pull-left" href="#" title="">
                          <i class="icon-twitter"></i>
                        </a>
                        <div class="media-body">
                          <h2 class="media-heading">Inspirational Hollywood Soundtracks for Your Halloween Video Project <a href='http://t.co/fzNYrib6gM' target='_blank'>http://t.co/fzNYrib6gM</a></h2>
                          <p>2 days ago</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="sliderItemWrapp">
                    <ul class="media-list">
                      <li class="media">
                        <a class="pull-left" href="#" title="">
                          <i class="icon-twitter"></i>
                        </a>
                        <div class="media-body">
                          <h2 class="media-heading"> <a href='https://twitter.com/musicmako'>@musicmako</a> Hello there! You just received a reply to that ticket. Can you check? ^MC</h2>
                          <p>3 days ago</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="sliderItemWrapp">
                    <ul class="media-list">
                      <li class="media">
                        <a class="pull-left" href="#" title="">
                          <i class="icon-twitter"></i>
                        </a>
                        <div class="media-body">
                          <h2 class="media-heading">RT  <a href='https://twitter.com/asellitt'>@asellitt</a> Awesome with a capital E  <a href='https://twitter.com/envato'>@envato</a> <a href='http://t.co/taZyQcCq0P' target='_blank'>http://t.co/taZyQcCq0P</a></h2>
                          <p>3 days ago</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="sliderItemWrapp">
                    <ul class="media-list">
                      <li class="media">
                        <a class="pull-left" href="#" title="">
                          <i class="icon-twitter"></i>
                        </a>
                        <div class="media-body">
                          <h2 class="media-heading">Convinced collis to turn his office into a  <a href='https://twitter.com/Foursquare'>@Foursquare</a> location ^ Josh @ Collis' Envato Office <a href='http://t.co/UiS8RRUwf4' target='_blank'>http://t.co/UiS8RRUwf4</a></h2>
                          <p>3 days ago</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="sliderItemWrapp">
                    <ul class="media-list">
                      <li class="media">
                        <a class="pull-left" href="#" title="">
                          <i class="icon-twitter"></i>
                        </a>
                        <div class="media-body">
                          <h2 class="media-heading">Envato Meetups: Last Chance to Register for LA &amp; SF! - <a href='http://t.co/6byx489qnQ' target='_blank'>http://t.co/6byx489qnQ</a></h2>
                          <p>3 days ago</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="sliderItemWrapp">
                    <ul class="media-list">
                      <li class="media">
                        <a class="pull-left" href="#" title="">
                          <i class="icon-twitter"></i>
                        </a>
                        <div class="media-body">
                          <h2 class="media-heading">RT  <a href='https://twitter.com/jordanlewiz'>@jordanlewiz</a> Just launched!  <a href='https://twitter.com/envato'>@envato</a> Marketplace Authors can now setup Automatic Monthly Withdrawals with a fancy new form :-) - http://…</h2>
                          <p>4 days ago</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="sliderItemWrapp">
                    <ul class="media-list">
                      <li class="media">
                        <a class="pull-left" href="#" title="">
                          <i class="icon-twitter"></i>
                        </a>
                        <div class="media-body">
                          <h2 class="media-heading"> <a href='https://twitter.com/adevlordfera'>@adevlordfera</a> Thanks! It was a long time in the making :D ^TK</h2>
                          <p>4 days ago</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="navarrows">
                  <a id="feedPrev" title="prev" href="#"><i class="icon-chevron-left"></i></a>
                  <a id="feedNext" title="next" href="#"><i class="icon-chevron-right"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="wrapper footer">
          <div id="googlemap"></div>
          <div class="exitMap">
            <i class="icon-remove-circle"></i>
          </div>
          <div class="full-bg map">
            <div class="container widgets">
              <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                  <div class="widgettitle">
                    <h1><i class="icon-pencil"></i> Recent posts</h1>
                  </div>
                  <ul class="media-list">
                    <li class="media">
                      <a href="#" title="">
                        <figure>
                          <img class="media-object" src="http://placehold.it/50x50" alt=""/>
                        </figure>
                        <div class="media-body">
                          <h2 class="media-heading">Default Text Post - clear magazine</h2>
                          <p>July 19, 2013 by Admin</p>
                        </div>
                      </a>
                    </li>
                    <li class="media">
                      <a href="#" title="">
                        <figure>
                          <img class="media-object" src="http://placehold.it/50x50" alt=""/>
                        </figure>
                        <div class="media-body">
                          <h2 class="media-heading">Default Image Post - beautiful life</h2>
                          <p>July 24, 2013 by Admin</p>
                        </div>
                      </a>
                    </li>
                    <li class="media">
                      <a href="#" title="">
                        <figure>
                          <img class="media-object" src="http://placehold.it/50x50" alt=""/>
                        </figure>
                        <div class="media-body">
                          <h2 class="media-heading">Default Audio Post - amazing</h2>
                          <p>September 21, 2013 by Lorem Ipsum</p>
                        </div>
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="clearfix visible-xs"></div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                  <div class="widgettitle">
                    <h1><i class="icon-user"></i> About</h1>
                  </div>
                  <p class="specification">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                  <span class="social-icon">
                    <a href="" title=""><i class="icon-facebook"></i></a>
                    <a href="" title=""><i class="icon-twitter"></i></a>
                    <a href="" title=""><i class="icon-google-plus"></i></a>
                    <a href="" title=""><i class="icon-pinterest"></i></a>
                  </span>
                </div>

                <div class="clearfix visible-xs"></div>
                <div class="clearfix visible-sm"></div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                  <div class="widgettitle">
                    <h1><i class="icon-globe"></i> Contact</h1>
                  </div>
                  <ul class="contact">
                    <li>Phone: (+48) 888 888 888</li>
                    <li>Email: <a href="#" title="">info@gmail.com</a> or <a href="#" title="">contact@gmail.com</a>
                    </li><br>
                    <li>LOGO INC.</li>
                    <li>Armii Krajowej, Świdnica</li>
                    <li><a href="https://maps.google.pl/maps?q=50.843698,16.486695&hl=pl&sll=50.843454,16.489577&sspn=0.026502,0.048022&t=h&z=17" target="_blank" class="show-on-map">Show on map</a></li>
                  </ul>
                </div>
                <div class="clearfix visible-xs"></div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                  <div class="widgettitle">
                    <h1><i class="icon-tag"></i> Site tags</h1>
                  </div>
                  <ul class="tagcloud">
                    <li><a href="#" title="">tagexample</a></li>
                    <li><a href="#" title="">tagexample</a></li>
                    <li><a href="#" title="">smalltag</a></li>
                    <li><a href="#" title="">smalltag</a></li>
                    <li><a href="#" title="">tagexample</a></li>
                    <li><a href="#" title="">tagexample</a></li>
                    <li><a href="#" title="">tagexample</a></li>
                    <li><a href="#" title="">tagexample</a></li>
                    <li><a href="#" title="">smalltag</a></li>
                    <li><a href="#" title="">smalltag</a></li>
                    <li><a href="#" title="">tagexample</a></li>
                    <li><a href="#" title="">tagexample</a></li>
                  </ul>
                </div>
                <div class="clearfix visible-xs"></div>
              </div>
              <div class="bg-opacity"><div class="bg-opacity-ripple"></div></div>
              <div class="clickOn"></div>
            </div>
          </div>
          <div class="full-bg">
            <footer>
              <div class="container">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 menu">
                    <ul class="footer-nav">
                      <li>
                        <a title="" href="#home">Home</a>
                      </li>
                      <li>
                        <a title="" href="#news">News</a>
                      </li>
                      <li>
                        <a title="" href="#features">Features</a>
                      </li>
                      <li>
                        <a title="" href="#portfolio">Portfolio</a>
                      </li>
                      <li>
                        <a title="" href="#team">Team</a>
                      </li>
                      <li>
                        <a title="" href="#clients">Testimonials</a>
                      </li>
                      <li>
                        <a title="" href="#blog">Blog</a>
                      </li>
                      <li>
                        <a title="" href="#contact">Contact</a>
                      </li>
                      <li class="copyrights">
                        © 2013 LOGO. All Rights Reserved.
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </footer>
          </div>
        </div>
      </div>
    <?php
      drupal_add_js(drupal_get_path('theme', 'plumacreativa') .'/js/jquery.min.js', array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js(drupal_get_path('theme', 'plumacreativa') .'/js/jquery.detect.min.js', array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js(drupal_get_path('theme', 'plumacreativa') .'/js/waypoints.min.js', array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js(drupal_get_path('theme', 'plumacreativa') .'/js/jquery.superslides.min.js', array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js(drupal_get_path('theme', 'plumacreativa') .'/js/jquery.carouFredSel-6.2.1.js',  array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js(drupal_get_path('theme', 'plumacreativa') .'/js/jquery.imagesloaded.js', array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js(drupal_get_path('theme', 'plumacreativa') .'/js/masonry.pkgd.min.js',array('type' => 'file', 'scope' => 'footer'));

      drupal_add_js('https://maps.googleapis.com/maps/api/js?key=AIzaSyAlDdTJbUxsgtp_bxEvx7AS2iJCjQY-ZcU&sensor=false', array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js(drupal_get_path('theme', 'plumacreativa') .'/js/jquery.touchswipe.min.js', array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js(drupal_get_path('theme', 'plumacreativa') .'/js/jquery.transit.min.js',  array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js(drupal_get_path('theme', 'plumacreativa') .'/js/jquery.throttle.js',array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js(drupal_get_path('theme', 'plumacreativa') .'/js/scrollspy.js', array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js(drupal_get_path('theme', 'plumacreativa') .'/js/main.js', array('type' => 'file', 'scope' => 'footer'));
      drupal_add_js(drupal_get_path('theme', 'plumacreativa') .'/js/color-switcher.js', array('type' => 'file', 'scope' => 'footer'));
    ?>

