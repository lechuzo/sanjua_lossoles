var mobile = jQuery.browser.mobile;

/**** GOOGLE MAPS ****/

if (mobile == false) {
	google.maps.visualRefresh = true;
	var center;

	function calculateCenter() {
		center = map.getCenter();
	}

	var map;
	var pointLocation = new google.maps.LatLng(51.103994, 17.010588);
	var pointLocationCen = new google.maps.LatLng(51.104411, 17.008633);
	var pointLocationRes = new google.maps.LatLng(51.104448,17.008911);
	var UIStatus = true;

	function initialize(UIStatus) {
		var mapOptions = {
			zoom: 17,
			center: pointLocationCen,
			disableDefaultUI: UIStatus,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById('googlemap'), mapOptions);

		var marker = new google.maps.Marker({
			position: pointLocation,
			map: map,
			title: "Hello World!"
		});
	}
	
	google.maps.event.addDomListener(window, 'load', initialize);
	
	google.maps.event.addDomListener(window, 'resize', function() {
		map.setCenter(pointLocationCen);
	});
}

/**** FUNCTIONS ****/

/**** CAROUSEL ****/

$(document).ready(function() {
	if ($('html').hasClass('ie9')) {
		$('.anim1').removeClass('anim1');
		$('.anim2').removeClass('anim2');
		$('.banner, .photo-people, .widgets .col-md-3 ').css('opacity', 1);
	}
});

function navSlider(sliderId, arrowId, scrollStep, direction) {
	$('#' + arrowId).on('click', function(e) {
		e.preventDefault();
		$('#' + sliderId).trigger(direction, scrollStep);
	});
}

function carousel(carouselId, countId, heightVar, containerClass, itemNumb, progressClass) {
	$('#' + carouselId).carouFredSel({
		width: '100%',
		items: {
			width: null,
			visible: {
				min: 1,
				max: itemNumb,
			},
			height: heightVar
		},
		auto: false,
		responsive: true,
		height: heightVar,
		onCreate: function() {
			countId = $('.' + containerClass + ' .sliderItemWrapp').length;
			$('.' + progressClass + ' .progressbar').width((100 / countId) + '%');
		},
		scroll: {
			items: 1,
			fx: 'fade',
			easing: 'swing',
			duration: 400,
			onBefore: function(data) {
				$('#' + carouselId).trigger('currentPosition', function(page) {
					$('.' + progressClass + ' .progressbar').width((100 / countId * (page + 1)) + '%');
				});
			},
		},
	}, { transition: false });
}

function mobileSlider(containerId) {

	var IMG_WIDTH = $('body').width();
	var currentImg = 0;
	var maxImages = $(containerId + ' .sliderItemWrapp').length;
	var speed = 500;

	var imgs;

	var swipeOptions = {
		triggerOnTouchEnd: true,
		swipeStatus: swipeStatus,
		allowPageScroll: "vertical",
		threshold: 75,
		excludedElements: '',
		tap: function(event, target) {
			$(target).click();
		}
	};

	$(function() {
		$(containerId).width(IMG_WIDTH);
		$(containerId + ' .mobile-slider').width(maxImages * IMG_WIDTH);
		$(containerId + ' .sliderItemWrapp').width(IMG_WIDTH - 20);
		imgs = $(containerId + " .mobile-slider");
		imgs.swipe(swipeOptions);
	});

	function swipeStatus(event, phase, direction, distance) {
		if (phase == "move" && (direction == "left" || direction == "right")) {
			var duration = 0;
			if (direction == "left")
				scrollImages((IMG_WIDTH * currentImg) + distance, duration);
			else if (direction == "right")
				scrollImages((IMG_WIDTH * currentImg) - distance, duration);
		}
		else if (phase == "cancel") {
			scrollImages(IMG_WIDTH * currentImg, speed);
		}
		else if (phase == "end") {
			if (direction == "right")
				previousImage();
			else if (direction == "left")
				nextImage();
		}
	}

	function previousImage() {
		currentImg = Math.max(currentImg - 1, 0);
		scrollImages((IMG_WIDTH - 10) * currentImg, speed);
	}

	function nextImage() {
		currentImg = Math.min(currentImg + 1, maxImages - 1);
		scrollImages((IMG_WIDTH - 10) * currentImg, speed);
	}

	function scrollImages(distance, duration) {
		imgs.css("-webkit-transition-duration", (duration / 1000).toFixed(1) + "s");
		var value = (distance < 0 ? "" : "-") + Math.abs(distance).toString();
		imgs.css("-webkit-transform", "translate3d(" + value + "px,0px,0px)");
	}
}

/**** ANIMATIONS ****/

function kitAnimation(time, element, typeanim) {
	$(element).each(function(i, that) {
		setTimeout(function() {
			$(that).addClass('uk-animation-' + typeanim);
		}, (time * i));
	});
}

function kitHover(classOrDOMelement, typeAnim) {
	$(' ' + classOrDOMelement).hover(function() {
		$(this).addClass('uk-animation-' + typeAnim);
	}, function() {
		$(this).removeClass('uk-animation-' + typeAnim);
	});
}

$(document).ready(function() {
	$('#splash-search-input').focusin(function() {
		$(this).css('top', '-10px');
		$('.splash-header .social-icon').stop(true, false).fadeOut();
	});

	$('#splash-search-input').focusout(function() {
		var that = this;
		setTimeout(function() {
			$(that).css('top', "-100px");
		}, 500);
		$('.splash-header .social-icon').stop(true, false).fadeIn();
	});

	$('.nav-search').click(function(e) {
		e.preventDefault();
		if (!$(this).hasClass('mobile-search-active')) {
			$(this).addClass('mobile-search-active');
			$('.mobile-search').addClass('active');
			$('.mobile-search input').focus();
		} else {
			$(this).removeClass('mobile-search-active');
			$('.mobile-search').removeClass('active');
		}
	});

	if (mobile == false) {
		// news carousel
		var newsCount = 0;
		carousel('newsSlide', newsCount, 'variable', 'news-container', 4, 'news');
		navSlider('newsSlide', 'newsNext', 1, 'next');
		navSlider('newsSlide', 'newsPrev', 1, 'prev');

		//features carousel
		var featCount = 0;
		carousel('featSlide', featCount, 'variable', 'feat-container', 3, 'features');
		navSlider('featSlide', 'featNext', 1, 'next');
		navSlider('featSlide', 'featPrev', 1, 'prev');

		//testimonials carousel
		var testimonialCount = 0;
		carousel('testimonialsSlide', testimonialCount, 'variable', 'testimonials-container', 2, 'testimonials');
		navSlider('testimonialsSlide', 'testimonialsNext', 1, 'next');
		navSlider('testimonialsSlide', 'testimonialsPrev', 1, 'prev');

		//clients carousel
		var clientCount = 0;
		carousel('clientSlide', clientCount, 'variable', 'clients-container', 6, 'clients');
		navSlider('clientSlide', 'clientNext', 1, 'next');
		navSlider('clientSlide', 'clientPrev', 1, 'prev');

		// feed carousel
		$('#feedSlide').carouFredSel({
			items: 1,
			auto: false,
			responsive: true,
			height: 'auto',
			scroll: {
				fx: 'fade',
				easing: 'swing',
				duration: 400,
				onBefore: function(data) {
					$(data.items.old).removeClass('animate-carousel-end');
					$(data.items.old).removeClass('animate-carousel');
					$(data.items.visible).addClass('animate-carousel');
				},
				onAfter: function(data) {
					$('.animate-carousel > div').each(function(i, that) {
						setTimeout(function() {
							$(that).addClass('run-anim');
						}, 200 * i);
					});
					$(data.items.old).find('.run-anim').removeClass('run-anim');
				}
			}
		});

		navSlider('feedSlide', 'feedNext', 1, 'next');
		navSlider('feedSlide', 'feedPrev', 1, 'prev');
	} else {
		mobileSlider('.news-container');
		mobileSlider('.feat-container');
		mobileSlider('.testimonials-container');
		mobileSlider('.clients-container');
		mobileSlider('.feed-container');
	}

	/**** map ****/
	$('div.clickOn').hover(function() {
		$('.bg-opacity').addClass('bg-opacity-hover');
	}, function() {
		$('.bg-opacity').removeClass('bg-opacity-hover');
	});

	$('div.clickOn, .show-on-map').on('click', function(e) {
		e.preventDefault();
		$('.bg-opacity').css({
			'-webkit-transform': 'scale(30,30)',
			'transform': 'scale(30, 30)',
			'-ms-transform': 'scale(30, 30)'
		});

		$('.widgets .row').css('opacity', 0);

		setTimeout(function() {
			$('#googlemap').css('z-index', '7');
			$('.exitMap').css({'display': 'block', 'z-index': '8'}).hide().delay(100).fadeIn();
		}, 700);
	});

	$('div .exitMap').on('click', function() {
		$(this).fadeOut(500);

		setTimeout(function() {
			$('.bg-opacity').removeAttr("style");
			map.panTo(pointLocationCen);
			map.setZoom(17);
		}, 300);

		setTimeout(function() {
			$('#googlemap').css('z-index', '0');
		}, 400);

		setTimeout(function() {
			$('.widgets .row').css('opacity', 1);
		}, 1000);
	});
});

/**** Blog masonry ****/
var blog = document.querySelector('.blog');
var blogcontainer = blog.querySelector('.blog-content');

$('.blog-content').imagesLoaded(function() {
	var blogmsnry = new Masonry(blogcontainer, {
		itemSelector: '.blog-content .article-content',
		columnWidth: '.grid-sizer',
	});

	jQuery(function() {
		var showmore = true;
		$('#blog-show-more').click(function(e) {
			e.preventDefault();
			if (showmore === false) {
				shakeButton(this);
				return false;
			}

			var elems = [];
			var blogId = 1;

			$.get('inc/blog-content/blog_more_' + blogId + '.php', function(data) {
				$('.blog-more-content').html(data);
				$('.blog-more-content .article-content').addClass('load-this').hide();
				$('.blog-more-content .article-content').each(function(i, item) {
					$('.blog-content').append(item);
					elems.push(item);
				});

				$('.blog-content .article-content.load-this').imagesLoaded(function() {
					blogmsnry.appended(elems);
					$('.load-this').show();
					blogmsnry.layout();
				});

				$('.blog-content .load-this').removeClass('load-this');
			}, 'html');

			showmore = false;
		});

		$('.blog .section-nav a').click(function(e) {
			e.preventDefault();
			$('.blog .section-nav .blog-selected').removeClass('blog-selected');
			$(this).addClass('blog-selected');
			showmore = true;
			var items = blogmsnry.getItemElements();
			var blogId = $(this).data('blog');
			blogmsnry.remove(items);

			$.get('inc/blog-content/blog_more_' + blogId + '.php', function(data) {

				var elems = [];
				$('.blog-more-content').html(data);
				$('.blog-more-content .article-content').addClass('load-this').hide();

				$('.blog-more-content .article-content').each(function(i, item) {
					$('.blog-content').append(item);
					elems.push(item);
				});

				$('.blog-content .article-content.load-this').imagesLoaded(function() {
					blogmsnry.appended(elems);
					$('.load-this').show();
					blogmsnry.layout();
				});

				$('.blog-content .load-this').removeClass('load-this');
			}, 'html');
		});
	});
});

function shakeButton(element) {
	$(element).addClass('uk-animation-shake').css({opacity: 1});
	setTimeout(function() {
		$(element).removeClass('uk-animation-shake');
	}, 200);
}

/**** Portfolio masonry ****/
var portfolio = document.querySelector('.portfolio-wrapper');
var portfolioContainer = portfolio.querySelector('.masonry-container');

jQuery(window).load(function() {
	var msnry = new Masonry(portfolioContainer, {
		itemSelector: '.masonry-container a',
		columnWidth: '.grid-sizer',
	});

	jQuery(function() {
		var showmore = true;
		$('#portfolio-show-more').click(function(e) {
			e.preventDefault();
			if (showmore === false) {
				shakeButton(this);
				return false;
			}

			var elems = [];
			var portfolioId = 1;

			$.get('inc/portfolio-content/portfolio_more_' + portfolioId + '.php', function(data) {
				var elems = [];
				$('.portfolio-more-content').html(data);
				$('.portfolio-more-content a').addClass('load-this').hide();
				$('.portfolio-more-content a').each(function(i, item) {
					$('.masonry-container').append(item);
					elems.push(item);
				});

				$('.masonry-container a.load-this').imagesLoaded(function() {
					msnry.appended(elems);
					$('.load-this').show();
					msnry.layout();
				});

				$('.masonry-container .load-this').removeClass('load-this');

			}, 'html');

			$('.portfolio-more-content a').each(function(i, data) {
				$('.masonry-container').append(data);
				elems.push(data);
			});

			msnry.appended(elems);
			showmore = false;
		});

		$('.portfolio .section-nav a').click(function(e) {
			e.preventDefault();
			$('.portfolio .section-nav .portfolio-selected').removeClass('portfolio-selected');
			$(this).addClass('portfolio-selected');
		});

		$('.portfolio-select').click(function(e) {
			e.preventDefault();
			showmore = true;
			var items = msnry.getItemElements();
			msnry.remove(items);
			var portfolioId = $(this).data('portfolio');
			$.get('inc/portfolio-content/portfolio_more_' + portfolioId + '.php', function(data) {
				var elems = [];
				$('.portfolio-more-content').html(data);
				$('.portfolio-more-content a').addClass('load-this').hide();
				$('.portfolio-more-content a').each(function(i, item) {
					$('.masonry-container').append(item);
					elems.push(item);
				});

				$('.masonry-container a.load-this').imagesLoaded(function() {
					msnry.appended(elems);
					$('.load-this').show();
					msnry.layout();
				});

				$('.masonry-container .load-this').removeClass('load-this');
			}, 'html');
		});
	});
});

/**** contact form ****/
$("textarea").focus(function() {
	$(this).closest(".message").find("label").addClass("highlight");
});

$("textarea").focusout(function() {
	$(this).closest(".message").find("label").removeClass("highlight");
});

/**** splash ****/
$('.reorder a').click(function(e) {
	if ($('body').hasClass('mobile-nav-show')) {
		$('body').removeClass('mobile-nav-show');
	} else {
		$('body').addClass('mobile-nav-show');
	}
});

var topoffset = $(".first-container").offset().top;
var $head = $('#ha-header');
var state = true;
var defaultScroll = true;

if (mobile == false) {
	$('.ha-waypoint').each(function(i) {
		var $el = $(this),
				animClassDown = $el.data('animateDown'),
				animClassUp = $el.data('animateUp');

		$el.waypoint(function(direction) {
			if (direction === 'down' && animClassDown && state == true) {
				$head.attr('class', 'ha-header ' + animClassDown);
				if (defaultScroll == true) {
					$('html, body').animate({scrollTop: 0});
				}

				state = false;
				setTimeout(function() {
					animateElements();
				}, 700);
			}
		}, {offset: '-150px'});
	});

	$('#back-to-splash').click(function(e) {
		e.preventDefault();
		$head.attr('class', 'ha-header ha-header-fullscreen');
		$('html, body').animate({ scrollTop: 0 });
		state = true;
		defaultScoll = true;
	});
}

$(function() {
	if (mobile == false) {
		$('#slides').superslides({
			hashchange: false,
			animation: 'slide',
			animation_speed: 700,
		});

		$(window).resize();
	}
});

$(function() {
	if (mobile == false) {
		var $window = $(window);
		var $body = $(document.body);
		$body.scrollspy({
			target: '#main-nav',
			offset: 100,
		});

		$window.on('load', function() {
			$body.scrollspy('refresh');
		});
	}
});

function changeProgressBar(elem) {
	var currentOffset = $(elem).offset().left;
	var elementWidth = $(elem).width();
	$('.navbar .progressbar').width(currentOffset + elementWidth);
}

$('#main-nav a, .splash-navigation a, .footer-nav a').click(function(e) {
	e.preventDefault();
	if($(this).hasClass('splash-close')) {
		$('#ha-header').removeClass('ha-header-fullscreen').addClass('ha-header-subfullscreen');
		setTimeout(function() {
			animateElements();
		}, 700);
	}

	defaultScroll = false;
	$('html, body').animate({
		scrollTop: $($(this).attr('href')).offset().top - 90
	}, 500);
});

if (mobile == false) {
	$(window).resize($.throttle(1000, function() {
		var elem = $('#main-nav .active a');
		changeProgressBar(elem);
	}));

	$(window).scroll($.throttle(200, function() {
		var elem = $('#main-nav .active a');
		changeProgressBar(elem);
	}));
}

//**** Animations ****/

function animateElements() {
	if (mobile == false) {
		$('.intro').waypoint(function() {
			kitAnimation(500, '#home .anim2', 'fade');
			kitAnimation(300, '#home .anim1', 'slide-top');
		}, {offset: '65%'});

		$('#news').waypoint(function() {
			$(this).find('.banner').addClass('uk-animation-fade');
			kitAnimation(300, '.news-container .anim2', 'fade');
		}, {offset: '65%'});

		$('#features').waypoint(function() {
			$(this).find('.banner').addClass('uk-animation-fade');
			kitAnimation(200, '.feat-container .anim1', 'scale-up');
		}, {offset: '65%'});

		$('.team').waypoint(function() {
			$(this).find('.banner').addClass('uk-animation-fade');
			kitAnimation(200, '.team .photo-people', 'scale-up');
		}, {offset: '65%'});

		$('#testimonials').waypoint(function() {
			$(this).find('.banner').addClass('uk-animation-fade');
			kitAnimation(200, '.testimonials-container .anim1', 'slide-left');
			kitAnimation(200, '.testimonials-container .anim2', 'slide-right');
		}, {offset: '65%'});

		$('#portfolio').waypoint(function() {
			$(this).find('.banner').addClass('uk-animation-fade');
			kitAnimation(200, '.portfolio-wrapper.anim1', 'fade');
		}, {offset: '65%'});

		$('#blog').waypoint(function() {
			$(this).find('.banner').addClass('uk-animation-fade');
			kitAnimation(200, '.blog .anim1', 'fade');
		}, {offset: '65%'});

		$('#clients').waypoint(function() {
			$(this).find('.banner').addClass('uk-animation-fade');
			kitAnimation(200, '.clients-container .anim1', 'slide-bottom');
		}, {offset: '65%'});

		$('.feed').waypoint(function() {
			$(this).find('.banner').addClass('uk-animation-fade');
		}, {offset: '65%'});

		$('#contact').waypoint(function() {
			$(this).find('.banner').addClass('uk-animation-fade');
			kitAnimation(300, '.contact-container .anim1', 'fade');
		}, {offset: '65%'});

		$('.footer').waypoint(function() {
			kitAnimation(300, '.footer .widgets .col-md-3', 'slide-bottom');
		}, {offset: '65%'});
	}
}

$(document).ready(function() {
	var animationType = 'easeInOutExpo';

	var checkEmail = function(em) {
		var emailRe = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return emailRe.test(em);
	};

	$('.contact-form').focus(function() {
		$(this).parent().removeClass('error');
	});

	$('#contact-form').submit(function(e) {
		e.preventDefault();
		$.ajax({
			url: 'inc/send.php',
			type: 'POST',
			dataType: 'json',
			data: $(this).serialize(),
			beforeSend: function() {
				var errors = false,
				validate = function() {
					var name = $('#form-name'),
					email = $('#form-email'),
					subject = $('#form-subject'),
					message = $('#form-message');

					errors = false;

					if(name.val().length === 0) {
						$('#errorName').removeClass('hidden');
						errors = true;
					} else {
						$('#errorName').addClass('hidden');
					}

					if(email.val().length === 0) {
						$('#errorEmail').removeClass('hidden');
						errors = true;
					} else if(!checkEmail(email.val())) {
						$('#errorEmail').removeClass('hidden');
						errors = true;
					} else {
						$('#errorEmail').addClass('hidden');
					}

					if(subject.val().length === 0) {
						$('#errorSubject').removeClass('hidden');
						errors = true;
					} else {
						$('#errorSubject').addClass('hidden');
					}

					if(message.val().length === 0) {
						$('#errorMessage').removeClass('hidden');
						errors = true;
					} else {
						$('#errorMessage').addClass('hidden');
					}
				};

				validate();

				if(errors) {
					return false;
				}
			}
		}).done(function(data) {
			if(data.success === true) {
				$('.alert-success').removeClass('hidden');
				$('#form-name').val('');
				$('#form-email').val('');
				$('#form-subject').val('');
				$('#form-message').val('');
			} else {
				// handle server validation here
			}
		}).fail(function() {
			// handle server fail here
		});
	});
});
