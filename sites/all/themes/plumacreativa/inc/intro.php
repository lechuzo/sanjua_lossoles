<div class="container first-container ha-waypoint" data-animate-down="ha-header-subfullscreen" data-animate-up="ha-header-fullscreen" id="home">
	<div class="intro anim1">
		<h2>Lorem ipsum dolor sit amet, <a title="" href="#">consectetur</a> adipiscing elit. 
			Integer fringilla <a title="" href="#">mauris</a> non 
			sapien vulputate, ac <a title="" href="#">dignissim</a> erat vehicula.</h2>
	</div>
	<div class="row ad">
		<div class="col-xs-12 col-sm-6 col-md-3 anim1">
			<div class="box boxicon">
				<div class="circle">
					<div class="state-first">
						<i class="icon-envelope-alt"></i>
					</div>					
					<div class="state-animated">
						<i class="icon-envelope-alt"></i>
					</div>					
				</div>	
				<h3>Fast <span>feedback</span></h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla
					mauris non sapien vulputate, ac dignissim erat vehicula. 
				</p>
				<button type="button" class="btn btn-default" title="">Read More</button>
			</div>
		</div>
		<div class="clearfix visible-xs"></div>
		<div class="col-xs-12 col-sm-6 col-md-3 anim1">
			<div class="box boxicon">
				<div class="circle">
					<div class="state-first">
						<i class="icon-lightbulb"></i>
					</div>					
					<div class="state-animated">
						<i class="icon-lightbulb"></i>
					</div>					
				</div>	
				<h3>Big <span>idea</span></h3>
				<p>
					Phasellus nisl dolor, molestie vitae sollicitudin eu, molestie sed velit. 
					Nam a lorem ornare, tempus felis at, interdum justo.
				</p>
				<button type="button" class="btn btn-default" title="">Read More</button>
			</div>
		</div>
		<div class="clearfix visible-sm"></div>
		<div class="clearfix visible-xs"></div>
		<div class="col-xs-12 col-sm-6 col-md-3 anim1">
			<div class="box boxicon">
				<div class="circle">
					<div class="state-first">
						<i class="icon-heart-empty"></i>
					</div>					
					<div class="state-animated">
						<i class="icon-heart-empty"></i>
					</div>					
				</div>	
				<h3>Graph <span>elements</span></h3>
				<p>
					Nulla commodo odio eget pharetra facilisis. Nam in erat facilisis, 
					sollicitudin massa et, malesuada lorem.
				</p>
				<button type="button" class="btn btn-default" title="">Read More</button>
			</div>
		</div>
		<div class="clearfix visible-xs"></div>
		<div class="col-xs-12 col-sm-6 col-md-3 anim1">
			<div class="box boxicon">
				<div class="circle">
					<div class="state-first">
						<i class="icon-eye-open"></i>
					</div>					
					<div class="state-animated">
						<i class="icon-eye-open"></i>
					</div>					
				</div>	
				<h3>Retina <span>ready</span></h3>
				<p>
					Phasellus iaculis, mauris ac vulputate bibendum, mi leo aliquet lectus, 
					ut auctor arcu justo malesuada ligula. 
				</p>
				<button type="button" class="btn btn-default" title="">Read More</button>
			</div>
		</div>
	</div>
</div>