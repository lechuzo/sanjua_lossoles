
<div class="container news" id="news">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="banner">
				<h3>News</h3>
				<div class="navarrows">
					<a id="newsPrev" title="prev" href="#"><i class="icon-chevron-left"></i></a>
					<a id="newsNext" title="next" href="#"><i class="icon-chevron-right"></i></a>
				</div>
				<div class="edge"></div>
				<div class="progressbar"></div>
			</div>
		</div>
	</div>

	<div class="row news-container mobile-slider-wrapper">
		<div id="newsSlide" class="mobile-slider">
			<div class="col-xs-12 col-sm-12 col-md-3 sliderItemWrapp">
				<div class="anim2 image-box">
					<a href="#" class="hover-news">
						<figure>
							<img src="http://placehold.it/280x195">
							<div class="news-description">
								<span class="title">Business graphic project</span>
								<span class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse viverra id mi sit amet pellentesque. Nulla id massa scelerisque, facilisis orci at, sodales sem. </span>
							</div>
						</figure>
					</a>
					<div class="photocorner">
						<i class="icon-pencil"></i>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 sliderItemWrapp">
				<div class="anim2 image-box">
					<a href="#" class="hover-news">
						<figure>
							<img src="http://placehold.it/280x195">
							<div class="news-description">
								<span class="title">Business graphic project</span>
								<span class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse viverra id mi sit amet pellentesque. Nulla id massa scelerisque, facilisis orci at, sodales sem. </span>
							</div>
						</figure>
					</a>
					<div class="photocorner">
						<i class="icon-camera"></i>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 sliderItemWrapp">
				<div class="anim2 image-box">
					<a href="#" class="hover-news">
						<figure>
							<img src="http://placehold.it/280x195">
							<div class="news-description">
								<span class="title">Business graphic project</span>
								<span class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse viverra id mi sit amet pellentesque. Nulla id massa scelerisque, facilisis orci at, sodales sem. </span>
							</div>
						</figure>
					</a>
					<div class="photocorner">
						<i class="icon-pencil"></i>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 sliderItemWrapp">
				<div class="anim2 image-box">
					<a href="#" class="hover-news">
						<figure>
							<img src="http://placehold.it/280x195">
							<div class="news-description">
								<span class="title">Business graphic project</span>
								<span class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse viverra id mi sit amet pellentesque. Nulla id massa scelerisque, facilisis orci at, sodales sem. </span>
							</div>
						</figure>
					</a>
					<div class="photocorner">
						<i class="icon-camera"></i>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 sliderItemWrapp">
				<div class="image-box">
					<a href="#" class="hover-news">
						<figure>
							<img src="http://placehold.it/280x195">
							<div class="news-description">
								<span class="title">Business graphic project</span>
								<span class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse viverra id mi sit amet pellentesque. Nulla id massa scelerisque, facilisis orci at, sodales sem. </span>
							</div>
						</figure>
					</a>
					<div class="photocorner">
						<i class="icon-pencil"></i>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 sliderItemWrapp">
				<div class="image-box">
					<a href="#" class="hover-news">
						<figure>
							<img src="http://placehold.it/280x195">
							<div class="news-description">
								<span class="title">Business graphic project</span>
								<span class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse viverra id mi sit amet pellentesque. Nulla id massa scelerisque, facilisis orci at, sodales sem. </span>
							</div>
						</figure>
					</a>
					<div class="photocorner">
						<i class="icon-camera"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
