<div class="full-bg feed">
	<div class="container ">
		<div class="row">
			<div class="banner col-xs-12 col-sm-12 col-md-12 feed-container mobile-slider-wrapper">
				<?php
				include 'inc/EpiTwitter/EpiCurl.php';
				include 'inc/EpiTwitter/EpiOAuth.php';
				include 'inc/EpiTwitter/EpiTwitter.php';

				$screen_name = "envato";
				$count = 10;

				// WARNING!!!
				// You need to generate Your own key (https://dev.twitter.com/apps), each key have limits of requests. I do use those keys on my demo pages
				// If You will use those keys it will probably stop working after some period of time...
				$consumerKey =  'ei1vfUuJImc5UAomb2KggQ';
				$consumerSecret = 'XrZZTfGkSGmmoD1paXlyAhnzYAG7qrm2vx6cZO0';
				$oauthToken =  '110240184-wz5hXurM43HEjb6mcWlKGUzwa7NxcAfCzAOfTXgO';
				$oauthSecret = '8fAJsRi2M50HnJONP4RcS3RYUmmcDgwws7CwE2GA';

				$twitterObj = new EpiTwitter($consumerKey, $consumerSecret, $oauthToken, $oauthSecret);
				$statuses = $twitterObj->get_statusesUser_timeline(array('screen_name'=>$screen_name, 'count'=>$count));

				function timeElapsed($ptime) {
					$etime = time() - $ptime;

					if ($etime < 1) {
						return '0 seconds';
					}

					$a = array( 12 * 30 * 24 * 60 * 60	=>  'year',
								30 * 24 * 60 * 60		=>  'month',
								24 * 60 * 60			=>  'day',
								60 * 60					=>  'hour',
								60						=>  'minute',
								1						=>  'second'
								);

					foreach ($a as $secs => $str) {
						$d = $etime / $secs;
						if ($d >= 1) {
							$r = round($d);
							return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
						}
					}
				}

				function replaceURLWithHTMLLinks($text) {
					$linksExp = '/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/i';
					$hashTagExp = '/#([\_\-A-Z0-9]+?)\:?\ /i';
					$userTagExp = '/@([\_\-A-Z0-9]+?)\:?\ /i';

					$text = preg_replace($linksExp, "<a href='$1' target='_blank'>$1</a>", $text);
					$text = preg_replace($hashTagExp, " <a href='https://twitter.com/search?q=%23$1&src=hash'>#$1</a> ", $text);
					$text = preg_replace($userTagExp, " <a href='https://twitter.com/$1'>@$1</a> ", $text);

					return $text;
				}
				?>
				<div id="feedSlide" class="mobile-slider">
					<?php
					foreach ($statuses->response as $tweet) :
					?>
					<div class="sliderItemWrapp">
						<ul class="media-list">
							<li class="media">
								<a class="pull-left" href="#" title="">
									<i class="icon-twitter"></i>
								</a>
								<div class="media-body">
									<h2 class="media-heading"><?php echo replaceURLWithHTMLLinks($tweet['text']); ?></h2>
									<p><?php echo timeElapsed(strtotime($tweet['created_at'])); ?></p>
								</div>
							</li> 
						</ul>
					</div>
					<?php
					endforeach;
					?>
				</div>

				<div class="navarrows">
					<a id="feedPrev" title="prev" href="#"><i class="icon-chevron-left"></i></a>
					<a id="feedNext" title="next" href="#"><i class="icon-chevron-right"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="wrapper footer">
	<div id="googlemap">
	</div>
	<div class="exitMap">
		<i class="icon-remove-circle"></i>
	</div>
	<div class="full-bg map">
		<div class="container widgets">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="widgettitle">
						<h1><i class="icon-pencil"></i> Recent posts</h1>
					</div>
					<ul class="media-list">
						<li class="media">
							<a href="#" title="">
								<figure>
									<img class="media-object" src="http://placehold.it/50x50" alt=""/>
								</figure>
								<div class="media-body">
									<h2 class="media-heading">Default Text Post - clear magazine</h2>
									<p>July 19, 2013 by Admin</p>
								</div>
							</a>
						</li>
						<li class="media">
							<a href="#" title="">
								<figure>
									<img class="media-object" src="http://placehold.it/50x50" alt=""/>
								</figure>
								<div class="media-body">
									<h2 class="media-heading">Default Image Post - beautiful life</h2>
									<p>July 24, 2013 by Admin</p>
								</div>
							</a>
						</li>
						<li class="media">
							<a href="#" title="">
								<figure>
									<img class="media-object" src="http://placehold.it/50x50" alt=""/>
								</figure>
								<div class="media-body">
									<h2 class="media-heading">Default Audio Post - amazing</h2>
									<p>September 21, 2013 by Lorem Ipsum</p>
								</div>
							</a>
						</li>	
					</ul>
				</div>
				<div class="clearfix visible-xs"></div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="widgettitle">
						<h1><i class="icon-user"></i> About</h1>
					</div>
					<p class="specification">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<span class="social-icon">
						<a href="" title=""><i class="icon-facebook"></i></a>
						<a href="" title=""><i class="icon-twitter"></i></a>
						<a href="" title=""><i class="icon-google-plus"></i></a>
						<a href="" title=""><i class="icon-pinterest"></i></a>
					</span>
				</div>

				<div class="clearfix visible-xs"></div>
				<div class="clearfix visible-sm"></div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="widgettitle">
						<h1><i class="icon-globe"></i> Contact</h1>
					</div>
					<ul class="contact">
						<li>Phone: (+48) 888 888 888</li>
						<li>Email: <a href="#" title="">info@gmail.com</a> or <a href="#" title="">contact@gmail.com</a>
						</li><br>
						<li>LOGO INC.</li>
						<li>Armii Krajowej, Świdnica</li>
						<li><a href="https://maps.google.pl/maps?q=50.843698,16.486695&hl=pl&sll=50.843454,16.489577&sspn=0.026502,0.048022&t=h&z=17" target="_blank" class="show-on-map">Show on map</a></li>
					</ul>
				</div>	
				<div class="clearfix visible-xs"></div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="widgettitle">	
						<h1><i class="icon-tag"></i> Site tags</h1>
					</div>
					<ul class="tagcloud">
						<li><a href="#" title="">tagexample</a></li>
						<li><a href="#" title="">tagexample</a></li>
						<li><a href="#" title="">smalltag</a></li>
						<li><a href="#" title="">smalltag</a></li>
						<li><a href="#" title="">tagexample</a></li>
						<li><a href="#" title="">tagexample</a></li>
						<li><a href="#" title="">tagexample</a></li>
						<li><a href="#" title="">tagexample</a></li>
						<li><a href="#" title="">smalltag</a></li>
						<li><a href="#" title="">smalltag</a></li>
						<li><a href="#" title="">tagexample</a></li>
						<li><a href="#" title="">tagexample</a></li>
					</ul>
				</div>
				<div class="clearfix visible-xs"></div>
			</div>
			<div class="bg-opacity"><div class="bg-opacity-ripple"></div></div>
			<div class="clickOn"></div>
		</div>

	</div>

	<div class="full-bg">	
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 menu">
						<ul class="footer-nav">
							<li>
								<a title="" href="#home">Home</a>
							</li>
							<li>
								<a title="" href="#news">News</a>
							</li>
							<li>
								<a title="" href="#features">Features</a>
							</li>
							<li>
								<a title="" href="#portfolio">Portfolio</a>
							</li>
							<li>
								<a title="" href="#team">Team</a>
							</li>
							<li>
								<a title="" href="#testimonials">Testimonials</a>
							</li>
							<li>
								<a title="" href="#blog">Blog</a>
							</li>
							<li>
								<a title="" href="#contact">Contact</a>
							</li>
							<li class="copyrights">
								© 2013 LOGO. All Rights Reserved.
							</li>
						</ul>
					</div>
				</div>
			</div>
		</footer>
	</div>
</div>
