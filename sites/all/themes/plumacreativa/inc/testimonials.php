<div class="container testimonials" id="testimonials">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="banner">
				<h3>Clients testimonials</h3>
				<div class="navarrows">
					<a id="testimonialsPrev" title="prev" href="#"><i class="icon-chevron-left"></i></a>
					<a id="testimonialsNext" title="next" href="#"><i class="icon-chevron-right"></i></a>
				</div>
				<div class="edge"></div>
				<div class="progressbar"></div>
			</div>
		</div>
	</div>
	<div class="row testimonials-container mobile-slider-wrapper">
		<div id="testimonialsSlide" class="mobile-slider">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 sliderItemWrapp anim1">
				<div class="col-xs-5 col-sm-5 col-md-5">
					<figure class="photo-people rot hover-img">
						<img src="http://placehold.it/120x120" alt="" />
						<img src="http://placehold.it/120x120" alt="" />
					</figure>
					<div class="data-person">
						<strong>John Doe</strong>
						<span>Microsoft inc. developer and ingeneer</span>
					</div>
				</div>
				<div class="col-xs-7 col-sm-7 col-md-7">
					<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "</p> 
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 sliderItemWrapp anim2">
				<div class="col-xs-5 col-sm-5 col-md-5">
					<figure class="photo-people rot hover-img">
						<img src="http://placehold.it/120x120" alt="" />
						<img src="http://placehold.it/120x120" alt="" />
					</figure>
					<div class="data-person">
						<strong>Katie Link</strong>
						<span>Microsoft inc. developer and ingeneer</span>
					</div>
				</div>
				<div class="col-xs-7 col-sm-7 col-md-7">
					<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p> 
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 sliderItemWrapp">
				<div class="col-xs-5 col-sm-5 col-md-5">
					<figure class="photo-people rot hover-img">
						<img src="http://placehold.it/120x120" alt="" />
						<img src="http://placehold.it/120x120" alt="" />
					</figure>
					<div class="data-person">
						<strong>John Doe</strong>
						<span>Microsoft inc. developer and ingeneer</span>
					</div>
				</div>
				<div class="col-xs-7 col-sm-7 col-md-7">
					<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "</p> 
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 sliderItemWrapp">
				<div class="col-xs-5 col-sm-5 col-md-5">
					<figure class="photo-people rot hover-img">
						<img src="http://placehold.it/120x120" alt="" />
						<img src="http://placehold.it/120x120" alt="" />
					</figure>
					<div class="data-person">
						<strong>Katie Link</strong>
						<span>Microsoft inc. developer and ingeneer</span>
					</div>
				</div>
				<div class="col-xs-7 col-sm-7 col-md-7">
					<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p> 
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 sliderItemWrapp">
				<div class="col-xs-5 col-sm-5 col-md-5">
					<figure class="photo-people rot hover-img">
						<img src="http://placehold.it/120x120" alt="" />
						<img src="http://placehold.it/120x120" alt="" />
					</figure>
					<div class="data-person">
						<strong>John Doe</strong>
						<span>Microsoft inc. developer and ingeneer</span>
					</div>
				</div>
				<div class="col-xs-7 col-sm-7 col-md-7">
					<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "</p> 
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 sliderItemWrapp">
				<div class="col-xs-5 col-sm-5 col-md-5">
					<figure class="photo-people rot hover-img">
						<img src="http://placehold.it/120x120" alt="" />
						<img src="http://placehold.it/120x120" alt="" />
					</figure>
					<div class="data-person">
						<strong>Katie Link</strong>
						<span>Microsoft inc. developer and ingeneer</span>
					</div>
				</div>
				<div class="col-xs-7 col-sm-7 col-md-7">
					<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p> 
				</div>
			</div>
		</div>
	</div>
</div>