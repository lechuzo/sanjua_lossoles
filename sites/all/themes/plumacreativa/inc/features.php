
<div class="container features" id="features">
	<div class="row">
		<div class=" col-sm-12 col-md-12">
			<div class="banner">
				<h3>Features</h3>
				<div class="navarrows">
					<a id="featPrev" title="prev" href="#"><i class="icon-chevron-left"></i></a>
					<a id="featNext" title="next" href="#"><i class="icon-chevron-right"></i></a>
				</div>
				<div class="edge"></div>
				<div class="progressbar"></div>
			</div>
		</div>
	</div>
	<div class="row feat-container mobile-slider-wrapper">
		<div id="featSlide" class="mobile-slider">
				<div class="col-md-4 sliderItemWrapp anim1">
					<div class="box boxicon">
						<div class="title-features">
							<i class="icon-desktop"></i>
						</div>	
						<h3>100% Responsive Design</h3>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
						</p>
						<button type="button" class="btn btn-default" title="">Read More</button>
					</div>
				</div>

				<div class="col-md-4 sliderItemWrapp anim1">
					<div class="box boxicon">
						<div class="title-features">
							<i class="icon-cog"></i>
						</div>
						<h3>High Quality</h3>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
						</p>
						<button type="button" class="btn btn-default" title="">Read More</button>
					</div>
				</div>

				<div class="col-md-4 sliderItemWrapp anim1">
					<div class="box boxicon">
						<div class="title-features">
							<i class="icon-file-text-alt"></i>
						</div>
						<h3>Extensive Documentation</h3>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
						</p>
						<button type="button" class="btn btn-default" title="">Read More</button>
					</div>
				</div>

				<div class="col-md-4 sliderItemWrapp">
					<div class="box boxicon">
						<div class="title-features">
							<i class="icon-desktop"></i>
						</div>
						<h3>100% Responsive Design</h3>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
						</p>
						<button type="button" class="btn btn-default" title="">Read More</button>
					</div>
				</div>

				<div class="col-md-4 sliderItemWrapp">
					<div class="box boxicon">
						<div class="title-features">
							<i class="icon-cog"></i>
						</div>
						<h3>High Quality</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
						</p>
						<button type="button" class="btn btn-default" title="">Read More</button>
					</div>
				</div>

				<div class="col-md-4 sliderItemWrapp">
					<div class="box boxicon">
						<div class="title-features">
							<i class="icon-file-text-alt"></i>
						</div>
						<h3>Extensive Documentation</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
						</p>
						<button type="button" class="btn btn-default" title="">Read More</button>
					</div>
				</div>

				<div class="col-md-4 sliderItemWrapp">
					<div class="box boxicon">
						<div class="title-features">
							<i class="icon-desktop"></i>

						</div>
						<h3>100% Responsive Design</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
						</p>
						<button type="button" class="btn btn-default" title="">Read More</button>
					</div>
				</div>

				<div class="col-md-4 sliderItemWrapp">
					<div class="box boxicon">
						<div class="title-features">
							<i class="icon-cog"></i>
						</div>
						<h3>High Quality</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
						</p>
						<button type="button" class="btn btn-default" title="">Read More</button>
					</div>
				</div>

				<div class="col-md-4 sliderItemWrapp">
					<div class="box boxicon">
						<div class="title-features">
							<i class="icon-file-text-alt"></i>
						</div>
						<h3>Extensive Documentation</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
						</p>
						<button type="button" class="btn btn-default" title="">Read More</button>
					</div>
				</div>
			</div>
	</div>
</div>
