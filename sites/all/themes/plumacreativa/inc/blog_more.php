			<div class="article-content">
				<div class="img-art">
					<figure class="hover-figure">
						<img src="img-280x195/1.jpeg" alt=""/>
						<div class="hover-figure-nav">
							<a href="#"><i class="icon-search"></i></a>
							<a href="#"><i class="icon-link"></i></a>
						</div>
					</figure>
					<div class="date-art">17.02</div>
				</div>
				<div class="title-art">
					<a href="#"><h1>Lorem ipsum</h1></a>
					<a href="#"><small>by Admin</small></a>
					<ul class="blog-tags">
						<li><a href="#" alt="">wordpress</a></li>
						<li><a href="#" alt="">programing</a></li>
						<li><a href="#" alt="">design</a></li>
					</ul>
				</div>
				<div class="spacing"></div>
				<div class="articles">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>

			<div class="article-content">
				<div class="img-art">
					<figure>
						<img src="http://placehold.it/280x195" alt=""/>
					</figure>
					<div class="date-art">16.02</div>
				</div>
				<div class="title-art">
					<a href="#"><h1>Lorem ipsum</h1></a>
					<a href="#"><small>by Admin</small></a>
					<ul class="blog-tags">
						<li><a href="#" alt="">no name</a></li>
						<li><a href="#" alt="">black</a></li>
						<li><a href="#" alt="">white</a></li>
						<li><a href="#" alt="">design</a></li>
					</ul>
				</div>
				<div class="spacing"></div>
				<div class="articles">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
			<div class="article-content">
				<div class="img-art">
					<figure>
						<img src="http://placehold.it/280x410" alt=""/>
					</figure>
					<div class="date-art">15.02</div>
				</div>
				<div class="title-art">
					<a href="#"><h1>Lorem ipsum</h1></a>
					<a href="#"><small>by Admin</small></a>
					<ul class="blog-tags">
						<li><a href="#" alt="">wordpress</a></li>
						<li><a href="#" alt="">design</a></li>
					</ul>
				</div>
				<div class="spacing"></div>
				<div class="articles">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
			<div class="article-content">
				<div class="img-art">
					<figure>
						<img src="http://placehold.it/280x195" alt=""/>
					</figure>
					<div class="date-art">14.02</div>
				</div>
				<div class="title-art">
					<a href="#"><h1>Lorem ipsum</h1></a>
					<a href="#"><small>by Admin</small></a>
					<ul class="blog-tags">
						<li><a href="#" alt="">wordpress</a></li>
						<li><a href="#" alt="">design</a></li>
					</ul>
				</div>
				<div class="spacing"></div>
				<div class="articles">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
			<div class="article-content">
				<div class="img-art">
					<figure>
						<img src="http://placehold.it/280x195" alt=""/>
					</figure>
					<div class="date-art">13.02</div>
				</div>
				<div class="title-art">
					<a href="#"><h1>Lorem ipsum</h1></a>
					<a href="#"><small>by Admin</small></a>
					<ul class="blog-tags">
						<li><a href="#" alt="">car</a></li>
						<li><a href="#" alt="">hot</a></li>
						<li><a href="#" alt="">cake</a></li>
						<li><a href="#" alt="">design</a></li>
					</ul>
				</div>
				<div class="spacing"></div>
				<div class="articles">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
			<div class="article-content">
				<div class="img-art">
					<figure>
						<img src="http://placehold.it/280x410" alt=""/>
					</figure>
					<div class="date-art">12.02</div>
				</div>
				<div class="title-art">
					<a href="#"><h1>Lorem ipsum</h1></a>
					<a href="#"><small>by Admin</small></a>
					<ul class="blog-tags">
						<li><a href="#" alt="">car</a></li>
						<li><a href="#" alt="">hot</a></li>
						<li><a href="#" alt="">cake</a></li>
						<li><a href="#" alt="">design</a></li>
					</ul>
				</div>
				<div class="spacing"></div>
				<div class="articles">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
			<div class="article-content">
				<div class="img-art">
					<figure>
						<img src="http://placehold.it/280x195" alt=""/>
					</figure>
					<div class="date-art">11.02</div>
				</div>
				<div class="title-art">
					<a href="#"><h1>Lorem ipsum</h1></a>
					<a href="#"><small>by Admin</small></a>
					<ul class="blog-tags">
						<li><a href="#" alt="">home</a></li>
						<li><a href="#" alt="">hot</a></li>
						<li><a href="#" alt="">lemon</a></li>
						<li><a href="#" alt="">design</a></li>
					</ul>
				</div>
				<div class="spacing"></div>
				<div class="articles">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>

			<div class="article-content">
				<div class="img-art">
					<figure>
						<img src="http://placehold.it/280x195" alt=""/>
					</figure>
					<div class="date-art">10.02</div>
				</div>
				<div class="title-art">
					<a href="#"><h1>Lorem ipsum</h1></a>
					<a href="#"><small>by Admin</small></a>
					<ul class="blog-tags">
						<li><a href="#" alt="">city</a></li>
						<li><a href="#" alt="">flower</a></li>
						<li><a href="#" alt="">design</a></li>
					</ul>
				</div>
				<div class="spacing"></div>
				<div class="articles">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
		</div>
	</div>
