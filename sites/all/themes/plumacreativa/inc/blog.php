<div class="container blog" id="blog">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="banner">
				<h3>Blog</h3>
				<div class="edge"></div>
				<div class="progressbar"></div>
				<ul class="section-nav">
					<li><a href="#" alt="" class="blog-selected" data-blog="1">all</a></li>
					<li><a href="#" alt="" data-blog="2">january</a></li>
					<li><a href="#" alt="" data-blog="3">february</a></li>
					<li><a href="#" alt="" data-blog="1">march</a></li>
					<li><a href="#" alt="" data-blog="2">april</a></li>
					<li><a href="#" alt="" data-blog="3">may</a></li>
					<li><a href="#" alt="" data-blog="1">june</a></li>
					<li><a href="#" alt="" data-blog="2">july</a></li>
					<li><a href="#" alt="" data-blog="3">august</a></li>
					<li><a href="#" alt="" data-blog="1">september</a></li>
					<li><a href="#" alt="" data-blog="2">october</a></li>
					<li><a href="#" alt="" data-blog="3">november</a></li>
					<li><a href="#" alt="" data-blog="1">december</a></li>
				</ul>
			</div>
		</div>
	</div>


	<div class="row blog-content anim1">
		<div class="grid-sizer"></div>

		<div class="article-content article-content-h1">
			<div class="img-art">
				<a href="#" class="hover-post">
					<figure>
						<img src="http://placehold.it/280x195" alt=""/>
					</figure>
				</a>
				<div class="date-art">17.02</div>
			</div>
			<div class="title-art">
				<a href="#"><h1>Lorem ipsum</h1></a>
				<a href="#"><small>by Admin</small></a>
				<ul class="blog-tags">
					<li><a href="#" alt="">wordpress</a></li>
					<li><a href="#" alt="">programing</a></li>
					<li><a href="#" alt="">design</a></li>
				</ul>
			</div>
			<div class="spacing"></div>
			<div class="articles">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
		</div>

		<div class="article-content article-content-h1">
			<div class="img-art">
				<a href="#" class="hover-post">
					<figure>
						<img src="http://placehold.it/280x195" alt=""/>
					</figure>
				</a>
				<div class="date-art">16.02</div>
			</div>
			<div class="title-art">
				<a href="#"><h1>Lorem ipsum</h1></a>
				<a href="#"><small>by Admin</small></a>
				<ul class="blog-tags">
					<li><a href="#" alt="">no name</a></li>
					<li><a href="#" alt="">black</a></li>
					<li><a href="#" alt="">white</a></li>
					<li><a href="#" alt="">design</a></li>
				</ul>
			</div>
			<div class="spacing"></div>
			<div class="articles">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			</div>
		</div>
		<div class="article-content article-content-h2">
			<div class="img-art">
				<a href="#" class="hover-post">
					<figure>
						<img src="http://placehold.it/280x410" alt=""/>
					</figure>
					<div class="date-art">15.02</div>
				</a>
			</div>
			<div class="title-art">
				<a href="#"><h1>Lorem ipsum</h1></a>
				<a href="#"><small>by Admin</small></a>
				<ul class="blog-tags">
					<li><a href="#" alt="">wordpress</a></li>
					<li><a href="#" alt="">design</a></li>
				</ul>
			</div>
			<div class="spacing"></div>
			<div class="articles">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
			</div>
		</div>
		<div class="article-content article-content-h1">
			<div class="img-art">
				<a href="#" class="hover-post">
					<figure>
						<img src="http://placehold.it/280x195" alt=""/>
					</figure>
				</a>
				<div class="date-art">14.02</div>
			</div>
			<div class="title-art">
				<a href="#"><h1>Lorem ipsum</h1></a>
				<a href="#"><small>by Admin</small></a>
				<ul class="blog-tags">
					<li><a href="#" alt="">wordpress</a></li>
					<li><a href="#" alt="">design</a></li>
				</ul>
			</div>
			<div class="spacing"></div>
			<div class="articles">
				<p>Labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
		</div>
		<div class="article-content article-content-h1">
			<div class="img-art">
				<a href="#" class="hover-post">
					<figure>
						<img src="http://placehold.it/280x195" alt=""/>
					</figure>
				</a>
				<div class="date-art">13.02</div>
			</div>
			<div class="title-art">
				<a href="#"><h1>Lorem ipsum</h1></a>
				<a href="#"><small>by Admin</small></a>
				<ul class="blog-tags">
					<li><a href="#" alt="">car</a></li>
					<li><a href="#" alt="">hot</a></li>
					<li><a href="#" alt="">cake</a></li>
					<li><a href="#" alt="">design</a></li>
				</ul>
			</div>
			<div class="spacing"></div>
			<div class="articles">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
		</div>
		<div class="article-content article-content-h2">
			<div class="img-art">
				<a href="#" class="hover-post">
					<figure>
						<img src="http://placehold.it/280x195" alt=""/>
					</figure>
				</a>
				<div class="date-art">12.02</div>
			</div>
			<div class="title-art">
				<a href="#"><h1>Lorem ipsum</h1></a>
				<a href="#"><small>by Admin</small></a>
				<ul class="blog-tags">
					<li><a href="#" alt="">car</a></li>
					<li><a href="#" alt="">hot</a></li>
					<li><a href="#" alt="">cake</a></li>
					<li><a href="#" alt="">design</a></li>
				</ul>
			</div>
			<div class="spacing"></div>
			<div class="articles">
				<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
			</div>
		</div>
		<div class="article-content article-content-h1">
			<div class="img-art">
				<a href="#" class="hover-post">
					<figure>
						<img src="http://placehold.it/280x195" alt=""/>
					</figure>
				</a>
				<div class="date-art">11.02</div>
			</div>
			<div class="title-art">
				<a href="#"><h1>Lorem ipsum</h1></a>
				<a href="#"><small>by Admin</small></a>
				<ul class="blog-tags">
					<li><a href="#" alt="">home</a></li>
					<li><a href="#" alt="">hot</a></li>
					<li><a href="#" alt="">lemon</a></li>
					<li><a href="#" alt="">design</a></li>
				</ul>
			</div>
			<div class="spacing"></div>
			<div class="articles">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
		</div>

		<div class="article-content article-content-h1">
			<div class="img-art">
				<a href="#" class="hover-post">
					<figure>
						<img src="http://placehold.it/280x195" alt=""/>
					</figure>
				</a>
				<div class="date-art">10.02</div>
			</div>
			<div class="title-art">
				<a href="#"><h1>Lorem ipsum</h1></a>
				<a href="#"><small>by Admin</small></a>
				<ul class="blog-tags">
					<li><a href="#" alt="">city</a></li>
					<li><a href="#" alt="">flower</a></li>
					<li><a href="#" alt="">design</a></li>
				</ul>
			</div>
			<div class="spacing"></div>
			<div class="articles">
				<p>Lorem ipsum dolor sit amet, tempor incididunt ut labore et dolore magna aliqua. Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
		</div>

	</div>
</div>
<div class="row blog-show-more-container">
	<button type="button" class="btn btn-default" id="blog-show-more">Show more...</button>	
</div>
<div class="hidden blog-more-content"></div>
