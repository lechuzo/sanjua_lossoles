<header id="ha-header" class="ha-header ha-header-fullscreen ">
	<div class="ha-header-perspective">
		<div class="ha-header-front">
			<div class="container splash-header">
				<img src="img/logotype.png"/>
				<span class="social-icon">
					<a href="" title=""><i class="icon-facebook"></i></a>
					<a href="" title=""><i class="icon-twitter"></i></a>
					<a href="" title=""><i class="icon-google-plus"></i></a>
					<a href="" title=""><i class="icon-pinterest"></i></a>
				</span>
				<div class="splash-search">
					<form>
						<label for="splash-search-input"><i class="icon-search"></i></label>
						<input type="text" placeholder="Search for..." id="splash-search-input"/>
					</form>
				</div>
			</div>
			<div id="slides">
				<ul class="slides-container">
					<li>
						<img src="http://placehold.it/1600x1200" alt="Cinelli" width="1600" height="1200"/>
						<div class="slide-content-wrapper">
							<div class="slide-content">
								<span class="slide-header">Excellent theme with excellent layouts</span>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis molestie magna. Fusce congue leo sit amet libero molestie, id blandit nunc pharetra. Praesent sed lobortis lacus, quis tempus tortor. Aenean vel euismod leo, a feugiat erat. In faucibus lorem condimentum turpis luctus, pharetra gravida odio vehicula. Proin sit amet dictum magna. 
								</p>
								<a href="#" class="splash-button">Read more</a>
							</div>
						</div>	
					</li>
					<li>
						<img src="http://placehold.it/1600x1200" width="1600" height="1200" alt="Surly"/>
						<div class="slide-content-wrapper">
							<div class="slide-content">
								<span class="slide-header">Fully responsive design</span>
								<p>
									Nullam aliquam ipsum sem, id euismod sem feugiat quis. Etiam ut neque sed urna bibendum feugiat. Quisque non consectetur dolor. Etiam blandit ultrices neque, et ullamcorper metus lobortis sed. Aliquam id quam eget justo auctor interdum vitae non massa. Ut eget mi eget felis elementum pulvinar. Sed orci mi, malesuada et purus quis, convallis consectetur ligula.
								</p>
								<a href="#" class="splash-button">Read more about mobile version</a>
							</div>
						</div>	  
					</li>
					<li>
						<img src="http://placehold.it/1600x1200" width="1600" height="1200" alt="Cinelli"/>
						<div class="slide-content-wrapper">
							<div class="slide-content">
								<span class="slide-header">SEO Friendly markup</span>
								<p>
									Duis ultrices porta dignissim. Mauris vitae tristique metus. Ut eu magna a eros aliquam lobortis eu nec erat. Vivamus at pulvinar justo. Donec feugiat orci feugiat condimentum convallis. Praesent elementum, enim ac rhoncus convallis, turpis ligula ornare dui, consectetur blandit sapien sem vel nibh. Phasellus non lectus vel nisl varius tristique.
								</p>
							</div>
						</div>	
					</li>
					<li>
						<img src="http://placehold.it/1600x1200" width="1600" height="1200" alt="Affinity"/>
						<div class="slide-content-wrapper">
							<div class="slide-content">
								<span class="slide-header">Simply awesome!</span>
								<p>
									Mauris quis libero id nibh faucibus adipiscing in eu diam. Sed vitae rutrum neque. Nunc dignissim consequat venenatis. Nullam lobortis enim a augue ultricies euismod. Etiam mollis odio non tortor tempor consequat. Cras condimentum vel velit quis venenatis. Cras ullamcorper a diam et tempus.
								</p>
								<a href="#" class="splash-button">Check out more features!</a>

							</div>
						</div>		  
					</li>
				</ul>

				<nav class="slides-navigation">
					<a href="#" class="next"><i class="icon-chevron-right"></i></a>
					<a href="#" class="prev"><i class="icon-chevron-left"></i></a>
				</nav>
			</div>

			<nav>
				<ul class="splash-navigation" style="display: table">
					<li>
						<a title="" href="#home" class="splash-close"><i class="icon-globe"></i> Home</a>
					</li>
					<li>
						<a title="" href="#news"><i class="icon-pencil"></i> News</a>
					</li>
					<li>
						<a title="" href="#features"><i class="icon-check"></i> Features</a>
					</li>
					<li>
						<a title="" href="#portfolio"><i class="icon-picture"></i> Portfolio</a>
					</li>
					<li>
						<a title="" href="#team"><i class="icon-group"></i> Team</a>
					</li>
					<li>
						<a title="" href="#testimonials"><i class="icon-comments-alt"></i> Testimonials</a>
					</li>
					<li>
						<a title="" href="#blog"><i class="icon-align-justify"></i> Blog</a>
					</li>
					<li>
						<a title="" href="#contact"><i class="icon-envelope-alt"></i> Contacts</a>
					</li>
				</ul>
			</nav>
		</div>
		<header class="navbar top ha-waypoint main-header">
			<div class="back-to-splash"></div>	

			<div class="container">
				<nav>
					<div class="row">
						<figure>
							<a href="#" id="back-to-splash"><img src="img/logo.png"></a>
						</figure>
						<div id="main-nav">
							<ul class="nav">
								<li class="active">
									<a title="" href="#home">Home</a>
								</li>
								<li>
									<a title="" href="#news">News</a>
								</li>
								<li>
									<a title="" href="#features">Features</a>
								</li>
								<li>
									<a title="" href="#portfolio">Portfolio</a>
								</li>
								<li>
									<a title="" href="#team">Team</a>
								</li>
								<li>
									<a title="" href="#testimonials">Testimonials</a>
								</li>
								<li>
									<a title="" href="#blog">Blog</a>
								</li>
								<li>
									<a title="" href="#contact">Contact</a>
								</li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
			<div class="edge"></div>
			<div class="progressbar"></div>
		</header>
	</div>
</header>
