<?php
	if (isset($_POST['form-name'])) {
		$validation = array();

		if (!isset($_POST['form-name']) || $_POST['form-name'] == '') {
			$validation[] = array('message'=>'Please enter Your Name', 'id'=>'form-email');
		}

		if (!isset($_POST['form-subject']) || $_POST['form-subject'] == '') {
			$validation[] = array('message'=>'Please enter subject', 'id'=>'form-subject');
		}

		if (!isset($_POST['form-email']) || $_POST['form-email'] == '') {
			$validation[] = array('message'=>'Please enter email', 'id'=>'form-subject');
		}

		if (isset($_POST['form-email']) && !filter_var($_POST['form-email'], FILTER_VALIDATE_EMAIL)) {
			$validation[] = array('message'=>'Please enter valid email', 'id'=>'form-email');
		}

		$subject = $_POST['form-subject'];
		$message = $_POST['form-message'];
		$headers = "";

		$email = "CHANGE_THIS@gmail.com";

		if (empty($validation)) {
			if (mail($email, $subject, $message, $headers)) {
				echo json_encode(array('success'=>(bool)true, 'message'=>''));
			} else {
				echo json_encode(array('success'=>(bool)false, 'type'=>'system', 'data'=>array('message'=>'Sending error, please try again later')));
			}
		} else {
			echo json_encode(array('success'=>(bool)false, 'type'=>'validation', 'data'=>$validation));
		}

		die();
	}
?>