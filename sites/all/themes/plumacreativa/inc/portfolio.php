<div class="container portfolio" id="portfolio">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="banner">
				<h3>Works</h3>
				<div class="edge"></div>	
				<div class="progressbar"></div>
				<ul class="section-nav">
					<li><a href="#" alt="" class="portfolio-select portfolio-selected" data-portfolio="1">all</a></li>
					<li><a href="#" alt="" class="portfolio-select" data-portfolio="2">branding</a></li>
					<li><a href="#" alt="" class="portfolio-select" data-portfolio="3">design</a></li>
					<li><a href="#" alt="" class="portfolio-select" data-portfolio="1">illustration</a></li>
					<li><a href="#" alt="" class="portfolio-select" data-portfolio="2">print</a></li>
					<li><a href="#" alt="" class="portfolio-select" data-portfolio="3">photo</a></li>
					<li><a href="#" alt="" class="portfolio-select" data-portfolio="1">user interface</a></li>
					<li><a href="#" alt="" class="portfolio-select" data-portfolio="2">video</a></li>
				</ul>				
			</div>
		</div>
	</div>
</div>
<div class="container portfolio-wrapper anim1">
	<div class="row">
	  <div id="basic" class="masonry-container">
		  <div class="grid-sizer"></div>
		  <?php include 'portfolio-content/portfolio_more_1.php';?>
	  </div>
	</div>
</div>
<div class="hidden portfolio-more-content"></div>
<div class="row portfolio-show-more-container">
	<button type="button" class="btn btn-default" id="portfolio-show-more">Show more...</button>	
</div>
