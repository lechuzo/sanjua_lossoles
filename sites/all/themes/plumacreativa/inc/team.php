<div class="container team" id="team">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="banner">
				<h3>Team</h3>
				<div class="edge"></div>
				<div class="progressbar"></div>
			</div>
		</div>
	</div>	
	<div class="">
		<div class="col-xs-6 col-sm-4 col-md-3">
			<figure class="photo-people">
				<img class="base-photo" src="http://placehold.it/120x120" alt=""/>
				<img class="color-photo" src="http://placehold.it/120x120" alt=""/>
			</figure>
			<div class="name">
				<strong>John</strong>
				<span>Doe</span>
			</div>
			<span class="status">Business</span>
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3">
			<figure class="photo-people">
				<img class="base-photo" src="http://placehold.it/120x120" alt=""/>
				<img class="color-photo" src="http://placehold.it/120x120" alt=""/>
			</figure>
			<div class="name">
				<strong>Kate</strong>
				<span>Link</span>
			</div>
			<span class="status">PR Manager</span>
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3">
			<figure class="photo-people">
				<img class="base-photo" src="http://placehold.it/120x120" alt=""/>
				<img class="color-photo" src="http://placehold.it/120x120" alt=""/>
			</figure>
			<div class="name">
				<strong>Alex</strong>
				<span>Nivald</span>
			</div>
			<span class="status">Product</span>
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3">
			<figure class="photo-people">
				<img class="base-photo" src="http://placehold.it/120x120" alt=""/>
				<img class="color-photo" src="http://placehold.it/120x120" alt=""/>
			</figure>
			<div class="name">
				<strong>Thomas</strong>
				<span>Morowsky</span>
			</div>
			<span class="status">Design</span>
		</div>
	</div>
	<div class="">
		<div class="col-xs-6 col-sm-4 col-md-3">
			<figure class="photo-people">
				<img class="base-photo" src="http://placehold.it/120x120" alt=""/>
				<img class="color-photo" src="http://placehold.it/120x120" alt=""/>
			</figure>
			<div class="name">
				<strong>Jacob</strong>
				<span>Fuhr</span>
			</div>
			<span class="status">Design</span>
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3">
			<figure class="photo-people">
				<img class="base-photo" src="http://placehold.it/120x120" alt=""/>
				<img class="color-photo" src="http://placehold.it/120x120" alt=""/>
			</figure>
			<div class="name">
				<strong>Peter</strong>
				<span>Firtz</span>
			</div>
			<span class="status">Development</span>
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3">
			<figure class="photo-people">
				<img class="base-photo" src="http://placehold.it/120x120" alt=""/>
				<img class="color-photo" src="http://placehold.it/120x120" alt=""/>
			</figure>
			<div class="name">
				<strong>Vlad</strong>
				<span>Caila</span>
			</div>
			<span class="status">Development</span>
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3">
			<figure class="photo-people">
				<img class="base-photo" src="http://placehold.it/120x120" alt=""/>
				<img class="color-photo" src="http://placehold.it/120x120" alt=""/>
			</figure>
			<div class="name">
				<strong>Alex</strong>
				<span>Drum</span>
			</div>
			<span class="status">Design</span>
		</div>
	</div>
</div>