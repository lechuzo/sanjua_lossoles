<div class="container clients" id="clients">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="banner">
				<h3>Clients</h3>
				<div class="navarrows">
					<a id="clientPrev" title="prev" href="#"><i class="icon-chevron-left"></i></a>
					<a id="clientNext" title="next" href="#"><i class="icon-chevron-right"></i></a>
				</div>
				<div class="edge"></div>
				<div class="progressbar"></div>
			</div>
		</div>
	</div>

	<div class="row clients-container mobile-slider-wrapper">
		<div id="clientSlide" class="mobile-slider">
			<div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp anim1">
				<figure>
					<img src="http://placehold.it/130x40" class="client-hover"/>
				</figure>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp anim1">
				<figure>
					<img src="http://placehold.it/130x40" class="client-hover">
				</figure>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp anim1">
				<figure>
					<img src="http://placehold.it/130x40" class="client-hover"/>
				</figure>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp anim1">
				<figure>
					<img src="http://placehold.it/130x40" class="client-hover"/>
				</figure>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp anim1">
				<figure>
					<img src="http://placehold.it/130x40" class="client-hover"/>
				</figure>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp anim1">
				<figure>
					<img src="http://placehold.it/130x40" class="client-hover"/>
				</figure>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp">
				<figure>
					<img src="http://placehold.it/130x40" class="client-hover"/>
				</figure>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp">
				<figure>
					<img src="http://placehold.it/130x40" class="client-hover"/>
				</figure>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp">
				<figure>
					<img src="http://placehold.it/130x40" class="client-hover"/>
				</figure>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp">
				<figure>
					<img src="http://placehold.it/130x40" class="client-hover"/>
				</figure>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp">
				<figure>
					<img src="http://placehold.it/130x40" class="client-hover"/>
				</figure>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-2 sliderItemWrapp">
				<figure>
					<img src="http://placehold.it/130x40" class="client-hover"/>
				</figure>
			</div>
		</div>
	</div>
</div>
