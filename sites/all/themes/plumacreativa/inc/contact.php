<div class="container contact" id="contact">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="banner">
				<h3>Contact</h3>
				<div class="edge"></div>
				<div class="progressbar"></div>
			</div>
		</div>
	</div>
	<div class="row contact-container">
		<div class="col-xs-12 col-sm-6 col-md-3">
			<ul class="data-company anim1">
				<li>Company</li>
				<li><b>Lorem Ipsum</b></li>
			</ul>
			<ul class="data-company anim1">
				<li>Adress</li>
				<li>Armii Krajowej 17</li>
				<li>Świdnica</li>
			</ul>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3">
			<ul class="data-company anim1">
				<li>Mobile</li>
				<li>+48 888 888 888</li>
			</ul>
			<ul class="data-company anim1">
				<li>E-mail</li>
				<li>info@gmail.com</li>
				<li>contact@gmail.com</li>
			</ul>
		</div>

		<form id="contact-form" class="anim1" action="send.php" method="POST">
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="input-wrapper">
					<input class="contact-form" type="text" placeholder="Name" name="form-name" id="form-name">			
					<span id="errorName" class="hidden error">Fill up name</span>
				</div>
				<div class="input-wrapper">
					<input class="contact-form" type="text" placeholder="E-mail" name="form-email" id="form-email"><div class="clearfix visible-xs"></div>
					<span id="errorEmail" class="hidden error">Fill up e-mail</span>
				</div>
				<div class="clearfix visible-xs"></div>
				<div class="input-wrapper">
					<input class="contact-form" type="text" placeholder="Subject" name="form-subject" id="form-subject"><div class="clearfix visible-xs"></div>
					<span id="errorSubject" class="hidden error">Fill up subject</span>
				</div>
				<div class="clearfix visible-xs"></div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="message">
					<div class="input-wrapper">
						<label for="text-message">Message</label>
						<div class="clearfix visible-xs"></div>
						<textarea class="contact-form" placeholder="Your message..." name="form-message" id="form-message"></textarea>
						<span id="errorMessage" class="hidden error">Fill up message</span>
					</div>
				</div>
				<div>
					<input type="submit" value="Send" class="buttonform">
				</div>
				<div class="alert alert-success hidden">Your message was sent. Thank you!</div>
			</div>
		</form>
	</div>
</div>
