<?php $my_path = drupal_get_path('theme', 'lossoles'); ?>
<div id="navbar-main"> 
  <!-- Fixed navbar -->
  <div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="navbar-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
       <object type="application/x-shockwave-flash" 
         data="<?php print $my_path ?>/img/logo.swf" 
         width="200" height="100">
         <param name="movie" value="<?php print $my_path ?>/img/logo.swf" />
         <param name="quality" value="high"/>
       </object></div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="#about-top"> Nosotros</a></li>
          <li><a href="#services-top"> Servicios</a></li>
          <li><a href="#clientes-top"> Clientes</a></li>
          <li><a href="#contacto-top"> Contacto</a></li>
        </ul>
      </div>
      <!--/.nav-collapse --> 
    </div>
  </div>
</div>

<!-- ==== HEADERWRAP ==== -->
<div id="headerwrap" name="home">
  <?php
    $block = module_invoke('views', 'block_view', 'slide-block');
    print render($block['content']);
  ?>
</div>
<!-- /headerwrap --> 
<div class="row" id="limpieza">
  <?php $block = module_invoke('views', 'block_view', 'limpieza_home-block');?>
  <h2 class="centered"><?php print render($block['subject']); ?></h2>
  <div class="row-content"><?php print render($block['content']); ?></div>
  <div id="about-top"></div>
</div>

<!-- ==== ABOUT ==== -->
<div id="about" name="about">
  <div class="container">
    <div class="row white">
      <?php $node = node_load(27); ?>
      <h2 class="centered"><?php print $node->title ?></h2>
      <div class="col-md-6">
        <?php print $node->body['und'][0]['value'] ?>
      </div>
    </div>
    <!-- row --> 
  </div>
  <div id="services-top"></div>
</div>
<!-- container --> 

<!-- ==== SERVICES ==== -->
<?php if ($page['servicios']): ?>
  <div id="services" class="clearfix" name="services">
    <div class="container">
      <div class="row">
        <?php print render($page['servicios']); ?>
        <div id="clientes-top"></div>
      </div>
    </div>
  </div> <!-- /#footer -->
<?php endif; ?>
<!-- container --> 

<!-- ==== PORTFOLIO ==== -->
<?php if ($page['footer']): ?>
  <div id="portfolio" class="clearfix"name="portfolio">
    <div class="container">
      <div class="row">
        <?php print render($page['clientes']); ?>
        <div id="contacto-top"></div>
      </div>
    </div>
  </div> <!-- /#clientes -->
<?php endif; ?>

<?php if ($page['contacto']): ?>
  <div id="contacto" class="clearfix">
    <?php print render($page['contacto']); ?>
  </div> <!-- /#footer -->
<?php endif; ?>

<?php if ($page['mapa']): ?>
  <div id="mapa" class="clearfix">
    <?php print render($page['mapa']); ?>
  </div> <!-- /#footer -->
<?php endif; ?>

<?php if ($page['pre_footer']): ?>
  <div id="pre-footer" class="clearfix">
    <?php print render($page['pre_footer']); ?>
  </div> <!-- /#footer -->
<?php endif; ?>

<?php if ($page['footer']): ?>
  <div id="footer" class="clearfix">
    <?php print render($page['footer']); ?>
  </div> <!-- /#footer -->
<?php endif; ?>


<script type="text/javascript" src="<?php print $my_path ?>/js/bootstrap.min.js"></script> 

<script type="text/javascript">
  setInterval(function() {
    $('.navbar-brand').effect('fade',900)
  }, 10);

  $('html, body').animate({
     scrollTop: $("head").offset().top}, 1000);
</script>