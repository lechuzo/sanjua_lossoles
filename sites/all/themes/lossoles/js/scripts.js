$(document).ready(function(){ 
  $("#edit-title").attr("placeholder", "Asunto").val("").focus().blur();
  $("#edit-field-contact-nombre-und-0-value").attr("placeholder", "Nombre").val("").focus().blur();
  $("#edit-field-contact-empresa-und-0-value").attr("placeholder", "Empresa a cargo").val("").focus().blur();
  $("#edit-field-contact-email-und-0-value").attr("placeholder", "Email").val("").focus().blur();
  $("#edit-body-und-0-value").attr("placeholder", "Mensaje").val("").focus().blur();
  $("#edit-field-contact-email-und-0-value--2").attr("placeholder", "Email").val("").focus().blur();

  $("#edit-title--2").attr("placeholder", "Asunto").val("").focus().blur();
  $("#edit-body-und-0-value--2").attr("placeholder", "Mensaje").val("").focus().blur();

  $("#edit-submit").val("Enviar");
  $("#edit-submit--2").val("Enviar");
  $("#edit-submit--3").val("Enviar");
  $("#edit-submit--4").val("Enviar");

  /** Efecto box hover **/
  $('.views-row-1 .views-field-nothing-1').css('display', 'none');
  $('.views-row-2 .views-field-nothing-1').css('display', 'none');
  $('.views-row-3 .views-field-nothing-1').css('display', 'none');
  $('.views-row-4 .views-field-nothing-1').css('display', 'none');

  $('.views-row-1 .views-field-nothing').hover(function(){
	  $(".view-limpieza-home .views-row-1 .views-field-nothing-1").show();
	  $(".view-limpieza-home .views-row-1 .views-field-nothing").hide();
	});

	$('.views-row-1 .views-field-nothing-1').mouseout(function(){
	  $(".view-limpieza-home .views-row-1 .views-field-nothing").show();
	  $(".view-limpieza-home .views-row-1 .views-field-nothing-1").hide();
	});

	$('.views-row-2 .views-field-nothing').hover(function(){
	  $(".view-limpieza-home .views-row-2 .views-field-nothing-1").show();
	  $(".view-limpieza-home .views-row-2 .views-field-nothing").hide();
	});

	$('.views-row-2 .views-field-nothing-1').mouseout(function(){
	  $(".view-limpieza-home .views-row-2 .views-field-nothing").show();
	  $(".view-limpieza-home .views-row-2 .views-field-nothing-1").hide();
	});

	$('.views-row-3 .views-field-nothing').hover(function(){
	  $(".view-limpieza-home .views-row-3 .views-field-nothing-1").show();
	  $(".view-limpieza-home .views-row-3 .views-field-nothing").hide();
	});

	$('.views-row-3 .views-field-nothing-1').mouseout(function(){
	  $(".view-limpieza-home .views-row-3 .views-field-nothing").show();
	  $(".view-limpieza-home .views-row-3 .views-field-nothing-1").hide();
	});

	$('.views-row-4 .views-field-nothing').hover(function(){
	  $(".view-limpieza-home .views-row-4 .views-field-nothing-1").show();
	  $(".view-limpieza-home .views-row-4 .views-field-nothing").hide();
	});

	$('.views-row-4 .views-field-nothing-1').mouseout(function(){
	  $(".view-limpieza-home .views-row-4 .views-field-nothing").show();
	  $(".view-limpieza-home .views-row-4 .views-field-nothing-1").hide();
	});
  
  $('ul.navbar-nav li').click(function(){
    $('ul.navbar-nav li').removeClass("active");
    $(this).addClass("active");
  });

}); 
