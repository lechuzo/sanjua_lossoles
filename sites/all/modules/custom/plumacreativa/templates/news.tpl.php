<?php foreach ($vars as $key => $item) : ?>
<div class="col-xs-12 col-sm-12 col-md-3 sliderItemWrapp">
  <?php
    if($key <= 3){
      $efect = 'anim2';
    } else {
      $efect = '';
    }
  ?>
  <div class="<?php print $efect ?> image-box">
    <a href="#" class="hover-news">
      <figure>
        <?php print show_image($item->field_news_image_fid) ?>
        <div class="news-description">
          <span class="title"><?php print $item->title ?></span>
          <span class="content"><?php print $item->body_value ?></span>
        </div>
      </figure>
    </a>
    <div class="photocorner">
      <i class="icon-pencil"></i>
    </div>
  </div>
</div>
<?php endforeach; ?>
