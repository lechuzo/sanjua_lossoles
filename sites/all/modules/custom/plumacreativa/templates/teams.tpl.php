<?php foreach ($vars as $key => $item) : ?>
  <div class="col-xs-6 col-sm-4 col-md-3">
    <figure class="photo-people">
      <?php print show_image($item->field_member_image_fid, 'base-photo') ?>
      <?php print show_image($item->field_member_image_fid, 'color-photo') ?>
    </figure>
    <div class="name">
      <strong><?php print $item->field_member_name_value ?></strong>
      <span><?php print $item->title ?></span>
    </div>
    <?php if($item->field_member_skill_value) {?><span class="status">
      <?php print $item->field_member_skill_value ?></span>
    <?php } ?>
    <div class="socials">
      <ul>
        <?php if($item->field_member_facebook_url) {?>
          <li class="facebook"><a href="<?php print $item->field_member_facebook_url ?>">Facebook</a></li>
        <?php } ?>
        <?php if($item->field_member_twitter_url) {?>
          <li class="twitter"><a href="<?php print $item->field_member_twitter_url ?>">Twitter</a></li>
        <?php } ?>
        <?php if($item->field_member_instagram_url) {?>
          <li class="instagram"><a href="<?php print $item->field_member_instagram_url ?>">Instagram</a></li>
        <?php } ?>
        <?php if($item->field_member_youtube_url) {?>
          <li class="youtube"><a href="<?php print $item->field_member_youtube_url ?>">You Tube</a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
<?php endforeach; ?>
