<?php foreach ($vars as $key => $item) : ?>

  <?php

    if($key == 0){
      $efect = 'mosaic-w2-h2';
    } elseif($key == 1) {
      $efect = 'mosaic-w2-h1';
    } elseif($key == 2) {
      $efect = 'mosaic-w1-h1';
    } elseif($key == 3) {
      $efect = 'mosaic-w1-h2';
    } elseif($key == 4) {
      $efect = 'mosaic-w1-h1';
    } elseif($key == 5) {
      $efect = 'mosaic-w1-h1';
    } elseif($key == 6) {
      $efect = 'mosaic-w1-h1';
    } else{
      $efect = 'mosaic-w1-h2';
    }
  ?>
  <a href="#" class="hover-portfolio <?php print $efect ?>">
    <figure>
      <?php print show_image($item->field_project_image_fid) ?>
      <div class="portfolio-description">
        <span class="title"><?php print $item->title ?></span>
        <span class="content"><?php print $item->body_value ?></span>
      </div>
    </figure>
  </a>
<?php endforeach; ?>
