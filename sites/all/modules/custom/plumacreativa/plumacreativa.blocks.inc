<?php

function plumacreativa_build_block($options, $delta){

  $vars = _plumacreativa_contents($options);

  switch ($delta) {
    case 'slide':
      return theme('slide', array('vars' => $vars));
      break;
    case 'news':
      return theme('news', array('vars' => $vars));
      break;
    case 'projects':
      return theme('projects', array('vars' => $vars));
      break;
    case 'team':
      return theme('teams', array('vars' => $vars));
      break;
    case 'testimonials':
      return theme('testimonials', array('vars' => $vars));
      break;
    case 'clients':
      return theme('clients', array('vars' => $vars));
      break;
    case 'blog':
      return theme('blogs', array('vars' => $vars));
      break;
    case 'twitter':
      return theme('twitter', array('vars' => $options));
      break;
  }
}
