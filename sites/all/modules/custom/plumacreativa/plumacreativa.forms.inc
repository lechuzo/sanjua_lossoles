<?php

/** Form Settings Default **/
function plumacreativa_settings_form($form, &$form_state){

  // Fieldset Socials Links
  $form['fieldset_socials'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social Networks'),
    '#description' => t('Custom your social links.'),
    '#collapsible' => TRUE,
  );

  $form['fieldset_socials']['facebook'] = array(
    '#title' => t('Facebook'),
    '#type' => 'textfield',
    '#default_value' => variable_get('settings_socials_facebook'),
    '#required' => TRUE,
  );

  $form['fieldset_socials']['twitter'] = array(
    '#title' => t('Twitter'),
    '#type' => 'textfield',
    '#default_value' => variable_get('settings_socials_twitter'),
  );

  $form['fieldset_socials']['instagram'] = array(
    '#title' => t('Instagram'),
    '#type' => 'textfield',
    '#default_value' => variable_get('settings_socials_instagram'),
  );

  // Fieldset Contact
  $form['fieldset_contact'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contact information'),
    '#description' => t('Customize your information the contact.'),
    '#collapsible' => TRUE,
  );

  $form['fieldset_contact']['company_name'] = array(
    '#title' => t('Company Name'),
    '#type' => 'textfield',
    '#default_value' => variable_get('settings_contact_company_name'),
  );

  $form['fieldset_contact']['company_address'] = array(
    '#title' => t('Company Address'),
    '#type' => 'textarea',
    '#default_value' => variable_get('settings_contact_company_address'),
  );

  $form['fieldset_contact']['company_mobile'] = array(
    '#title' => t('Company Mobile'),
    '#type' => 'textfield',
    '#default_value' => variable_get('settings_contact_company_mobile'),
    '#required' => TRUE,
  );

  $form['fieldset_contact']['company_email'] = array(
    '#title' => t('Company Email'),
    '#type' => 'textfield',
    '#default_value' => variable_get('settings_contact_company_email'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#value' => t('Submit'),
    '#type' => 'submit',
  );

  return $form;
}

function plumacreativa_settings_form_submit($form, &$form_state){
  // Socials variables
  variable_set('settings_socials_facebook', $form_state['values']['facebook']);
  variable_set('settings_socials_twitter', $form_state['values']['twitter']);
  variable_set('settings_socials_instagram', $form_state['values']['instagram']);

  // Socials Contact Info
  variable_set('settings_contact_company_name', $form_state['values']['company_name']);
  variable_set('settings_contact_company_address', $form_state['values']['company_address']);
  variable_set('settings_contact_company_mobile', $form_state['values']['company_mobile']);
  variable_set('settings_contact_company_email', $form_state['values']['company_email']);
}
