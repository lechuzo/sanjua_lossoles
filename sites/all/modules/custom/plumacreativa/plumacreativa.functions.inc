<?php

// create image
function show_image($fid, $attr_class = NULL){
  $imgpath = file_load($fid)->uri;
  $image_path = file_create_url($imgpath);
  print theme_image(array('path' => $image_path, 'attributes' => array('class' => $attr_class)));
}
