<?php

function _plumacreativa_contents($options){
  $query = db_select('node', 'n');

  // filter fields per content
  if($options['type'] == 'slide'){
    $query -> leftJoin('field_data_field_slide_image', 'sli', 'sli.entity_id = n.nid');
    $query -> leftJoin('field_data_body', 'b', 'b.entity_id = n.nid');
  }
  if($options['type'] == 'news'){
    $query -> leftJoin('field_data_field_news_image', 'nei', 'nei.entity_id = n.nid');
    $query -> leftJoin('field_data_field_tags', 'tag', 'tag.entity_id = n.nid');
    $query -> leftJoin('field_data_body', 'b', 'b.entity_id = n.nid');
  }
  if($options['type'] == 'project'){
    $query -> leftJoin('field_data_body', 'b', 'b.entity_id = n.nid');
    $query -> leftJoin('field_data_field_project_image', 'pri', 'pri.entity_id = n.nid');
    $query -> leftJoin('field_data_field_project_category', 'prc', 'prc.entity_id = n.nid');
    $query -> leftJoin('field_data_field_reference_client', 'rec', 'rec.entity_id = n.nid');
  }
  if($options['type'] == 'member'){
    $query -> leftJoin('field_data_field_member_name', 'men', 'men.entity_id = n.nid');
    $query -> leftJoin('field_data_field_member_image', 'mei', 'mei.entity_id = n.nid');
    $query -> leftJoin('field_data_field_member_skill', 'mes', 'mes.entity_id = n.nid');
    $query -> leftJoin('field_data_field_member_facebook', 'mfa', 'mfa.entity_id = n.nid');
    $query -> leftJoin('field_data_field_member_twitter', 'mtw', 'mtw.entity_id = n.nid');
    $query -> leftJoin('field_data_field_member_instagram', 'min', 'min.entity_id = n.nid');
    $query -> leftJoin('field_data_field_member_youtube', 'myt', 'myt.entity_id = n.nid');
  }
  if($options['type'] == 'testimonials'){
    $query -> leftJoin('field_data_field_testimonials_image', 'tei', 'tei.entity_id = n.nid');
    $query -> leftJoin('field_data_field_reference_client', 'rec', 'rec.entity_id = n.nid');
  }
  if($options['type'] == 'blog'){
    $query -> leftJoin('field_data_field_blog_image', 'bli', 'bli.entity_id = n.nid');
    $query -> leftJoin('field_data_field_tags', 'tag', 'tag.entity_id = n.nid');
  }


  // fields to show
  $query = $query->fields('n' , array('title', 'nid', 'created'));

  // add fields per content
  if($options['type'] == 'slide'){
    $query = $query->fields('sli', array('field_slide_image_fid'));
    $query = $query->fields('b', array('body_value'));
  }
  if($options['type'] == 'news'){
    $query = $query->fields('nei', array('field_news_image_fid'));
    $query = $query->fields('tag', array('field_tags_tid'));
    $query = $query->fields('b', array('body_value'));
  }
  if($options['type'] == 'project'){
    $query = $query->fields('b', array('body_value'));
    $query = $query->fields('pri', array('field_project_image_fid'));
    $query = $query->fields('prc', array('field_project_category_tid'));
    $query = $query->fields('rec', array('field_reference_client_target_id'));
  }
  if($options['type'] == 'member'){
    $query = $query->fields('men', array('field_member_name_value'));
    $query = $query->fields('mei', array('field_member_image_fid'));
    $query = $query->fields('mes', array('field_member_skill_value'));
    $query = $query->fields('mfa', array('field_member_facebook_url'));
    $query = $query->fields('mtw', array('field_member_twitter_url'));
    $query = $query->fields('min', array('field_member_instagram_url'));
    $query = $query->fields('myt', array('field_member_youtube_url'));
  }
  if($options['type'] == 'testimonials'){
    $query = $query->fields('tei', array('field_testimonials_image_fid'));
    $query = $query->fields('rec', array('field_reference_client_target_id'));
  }
  if($options['type'] == 'blog'){
    $query = $query->fields('bli', array('field_blog_image_fid'));
    $query = $query->fields('tag', array('field_tags_tid'));
  }

  // filter per type
  $query = $query->condition('n.type', $options['type']);

  // order by date
  $query = $query->OrderBy('created', 'DESC');

  $query = $query
    ->condition('n.status', 1)
    ->range(0, $options['range'])
    ->execute()
    ->fetchAll();

  //echo '<pre>';
  //print_r($query);
  //echo '</pre>';

  return $query;
}
